// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          http://www.boost.org/LICENSE_1_0.txt)

#include <math.h>
#include <vector>
#define _USE_MATH_DEFINES 

#include <omp.h>
#include <openacc.h> //on Win I need to comment it out for now

#include <boost/math/special_functions/bessel.hpp>
#include <boost/math/special_functions/gamma.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/binomial_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/gamma_distribution.hpp>

#include <gperftools/profiler.h> // comment out if not needed

#define ARMA_DONT_PRINT_ERRORS // dangerous but i hate the messages overflow ... ^^

#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins(openmp)]]
// [[Rcpp::depends(BH)]]

boost::mt19937 rng;
omp_lock_t RNGlock;
int GLOBAL_M = 20; // BAD PRACTICE, TO FIX

const double ni_likelihood = 1./2.;
const double ni_prior = 1./2.; //smoothness of the covariance for Z 
const double nu_y=ni_likelihood, nu_z=ni_prior;

using namespace arma;

boost::random::uniform_real_distribution<> random_01(0.,1.);
boost::random::normal_distribution<> random_normal(0.,1.);

template <typename T> int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

double w_var(vec v,vec w){ // should probably use compensated algorithms or more stable versions at least... (for numerical errors..)
  //int n = v.n_elem;
  double v_bar = sum(v%w)/sum(w);
  
  return sum( w % pow(v-v_bar,2) )/sum(w);
}

double w_cov(vec v1, vec v2, vec w){
  //int n = v1.n_elem;
  double v1_bar = sum(v1%w)/sum(w);
  double v2_bar = sum(v2%w)/sum(w);
  
  return sum( w % ((v1-v1_bar)%(v2-v2_bar)) )/sum(w);
}

vec unif_01(int size){
  vec ret(size);
  for(int i=0;i<size;++i) ret(i) = random_01(rng);
  return ret;
}

vec normal_01(int size){
  vec ret(size);
  for(int i=0;i<size;++i) ret(i) = random_normal(rng);
  return ret;
}

vec random_gamma(int size,boost::random::gamma_distribution<> random ){
  vec ret(size);
  for(int i=0;i<size;++i) ret(i) = random(rng);
  return ret;
}

int rbinomial(int size, double prob){
  boost::random::binomial_distribution<int> binomialrv(size,prob);
  return binomialrv(rng);
}

uvec rmultinomial(int n,vec prob){
  
  int K = prob.n_elem;
  uvec rN = zeros<uvec>(K);
  double p_tot = sum(prob);
  double pp;
  
  for(int k = 0; k < K-1; k++) {
    if(prob(k)>0) {
      pp = prob[k] / p_tot;
      rN(k) = ((pp < 1.) ? rbinomial(n,  pp) : n);
      n -= rN[k];
    }else rN[k] = 0;
    
    if(n <= 0) /* we have all*/ return rN;
    p_tot -= prob[k]; /* i.e. = sum(prob[(k+1):K]) */
  }
  rN[K-1] = n - sum(rN);
  return rN;
}

double dlnorm(double x, double meanlog, double sdlog, int give_log)
{
  double y;
  
  if(sdlog <= 0) return 0;
  
  if(x <= 0) return 0;
  
  y = (log(x) - meanlog) / sdlog;
  return (give_log ?
          -(log(sqrt(2*M_PI)) + 0.5 * y * y + log(x * sdlog)) :
          1/sqrt(2*M_PI) * exp(-0.5 * y * y)  /   (x * sdlog));
  
}


uvec multi_resample(int n,vec prob){
  uvec idx(n);
  
  uvec Ni = rmultinomial(n,prob); // get how many rep of each gets resampled
  
  uvec tmp = nonzeros(Ni);
  cout << "Kept " << tmp.n_elem << " points over "<< prob.n_elem << " ... " ;
  
  int i=0;
  for(int j=0;j<n;++j){
    
    while(Ni(i) <= 0){
      ++i;
    }
    
    idx(j) = i;
    Ni(i) = Ni(i)-1;
    
  }
  
  return idx;
  
}

uvec resid_resample(int n,vec prob){
  
  uvec idx(n);
  
  uvec Ni = conv_to<uvec>::from( floor( (double)n * prob) );

  std::cout << "Fixed " << sum(Ni) << " points .. ";
  prob = ((double)n*prob - floor( (double)n * prob) ) / ((double)n - (double)sum(Ni));

  Ni += rmultinomial(n-sum(Ni), prob ); 

  uvec tmp = nonzeros(Ni);
  cout << "Kept " << tmp.n_elem << " points over "<< prob.n_elem << " ... " ;
  
  int i=0;
  for(int j=0;j<n;++j){
    
    while(Ni(i) <= 0){
      ++i;
    }
    
    idx(j) = i;
    Ni(i) = Ni(i)-1;
    
  }
  
  return idx;
  
}

//prior for P
vec random_prior_p(int size,boost::random::uniform_int_distribution<> random ){ // could we make it generic? Is it possible that something similr does not exists already?
  vec ret(size);
  for(int i=0;i<size;++i) ret(i) = random(rng);
  return ret;
}
// end prior for P

// Proposal for p
vec p_proposal(int p,int min_p,int max_p){ //proposal or p, the extra-dimensions  //TODO, the base model where p=0 should be taken into account
  
  double rand = as_scalar(unif_01(1));
  vec new_p(2);
  
  if(p == min_p){
    
    new_p(0) = min_p+1;
    new_p(1) = 1./0.5;
    
  }else{ //p(i) > min_p
    
    if( p == max_p ){
      
      new_p(0) = max_p-1;
      new_p(1) = 1./0.5;
      
    }else{ // p between 1 and max_p-1
      if( rand < 0.5 ){
        new_p(0) = p-1;
        new_p(1) =  ( ( new_p(0) == min_p ) ? 0.5/1. : 0.5/0.5 );
      }else{
        new_p(0) = p+1;
        new_p(1) =  ( ( new_p(0) == max_p ) ? 0.5/1. : 0.5/0.5 );
      }
      
    }
    
  }
  
  return new_p;
  
}
// end proposal for p


double log_add(double a, double b){
  
  if(a>=b){
    return a+log(1+exp(b-a));
  }else{
    return b+log(exp(a-b)+1);
  }
  
}


double vec_log_add(arma::vec v){
  
  double m = arma::min(v);  // I suppose I'm working with probabilities, hence with negatives log_values
  double M = arma::max(v);
  double ret = M;
  
  v = v( find(v!=m) );
  
  for(int i=0; i<v.n_elem; ++i) ret += log(1+exp(m-v(i)));
  
  return ret;
  
}

double ESS_log(vec lW){
  
  int n = lW.n_elem;
  
  // Compute the sum of the w and the sum of the squared w in the log scale
  double sum_lW = lW(0);
  double sum_lWs = 2*lW(0);
  
  for(int i=1;i<n;i++){
    
    sum_lW = log_add(sum_lW,lW(i));
    sum_lWs = log_add(sum_lWs,2*lW(i));
    
  }
  
  // Compute ESS
  // sum(W)^2 / sum(W^2) = sum(W)^2 * sum(W^2)^-1 = exp(2*sum_lW - sum_lWs)
  
  double ess = exp(2*sum_lW - sum_lWs);
  return ess;
  
}

double cESS_log(vec liw, vec prevlW){ // liw log incremental weights, prevlW old log normalized weights 
  
  int n = liw.n_elem;
  vec lNum(n); vec lDen(n);

  lNum = prevlW + liw;
  lDen = prevlW + 2*liw; 

  // Compute the sum of the w and the sum of the squared w in the log scale
  double sum_lDen = lDen(0);
  double sum_lNum = lNum(0);
  
  for(int i=1;i<n;i++){
    
    sum_lDen = log_add(sum_lDen,lDen(i));
    sum_lNum = log_add(sum_lNum,lNum(i));
    
  }
  
  // Compute cESS
  
  double cess = exp(log(n) + 2*sum_lNum - sum_lDen);
  return cess;
  
}

double bisection(vec lw,vec ll,double t_ref,double t_up,double t_down,double ess_up,double ess_down,double thresh){
  
  double t_mid,ess_mid;
  double eps = 1.; //std::numeric_limits< double >::min(); //this is just too much...
  // bool unable;
  
  int it=0; int max_it=1000;
  
  while( ( (t_up-t_down) > (1e3*DBL_MIN) ) & (it < max_it) ){ //anyway it is never reached, is there just to constraint maxit
    
    t_mid = (t_up+t_down)*0.5;
    
    ess_mid = ESS_log(lw + ((t_ref/t_mid -1.)*ll) );
    
    if( std::abs(ess_mid - thresh) <= eps ) return t_mid;
    
    if( sgn(ess_up - thresh) == sgn(ess_mid - thresh) ){
      // return bisection(lw,ll,t_ref,t_mid,t_down,ess_mid,ess_down,thresh);
      t_up = t_mid;
      ess_up = ess_mid;
    }else{
      // return bisection(lw,ll,t_ref,t_up,t_mid,ess_up,ess_mid,thresh);
      t_down = t_mid;
      ess_down = ess_mid;
    }
    
    ++it;
    
  }
  
  // return t_up;
  
  if(ess_down <= 5 ) return t_up;
  else{ 
    t_mid = (t_up+t_down)*0.5;
    ess_mid = ESS_log(lw + ((t_ref/t_mid -1.)*ll) );
    if( std::isfinite(ess_mid) && ess_mid!=0) return t_mid;
    else return t_up;
  }
  
}

double find_temp(vec lw,vec ll,double t_ref,double ess_ref,double thresh){
  //braketing
  // IMPORTANT: REMEMBER TO PASS TEMPERED LIKELIHOOD wrt T_REF AND LOG_WEIGHTS!
  
  double t_up = t_ref;
  double ess_up = ess_ref;
  int n_int = 100;
  double int_length = (t_up-1.0)/(double)n_int;
  double ess_down,t_down;
  
  int i=0;
  while(i<n_int){
    t_down = t_up - int_length;
    ess_down = ESS_log(lw + ((t_ref/t_down)*ll - ll ));
    
    if(sgn(ess_up - thresh) != sgn(ess_down - thresh)){
      std::cout<<" Found braketing in "<<t_up<<"( "<<ess_up<<" ) - "<<t_down<<"( "<<ess_down<<" ) ... ";
      return bisection(lw,ll,t_ref,t_up,t_down,ess_up,ess_down,thresh);
    }
    
    i++;
    t_up = t_down;
    ess_up = ess_down;
  }
  
  //last hope, the root is nowhere because in 1 we are still over thresh!
  if( (ess_down - thresh) >= 0 ) return 1.0;
  
  //if not...
  std::cout<<"Something went wrong int the bisection..."<<endl;
  return 0.0000001;  //will get the ESS to 0 and lead to complete degeneracy.
}

double bisection_cESS(vec lW,vec ll,double t_ref,double t_up,double t_down,double ess_up,double ess_down,double thresh){
  
  //care no check here for normalization of weights

  double t_mid,ess_mid;
  double eps = 1.; //std::numeric_limits< double >::min(); //this is just too much...
  // bool unable;
  
  int it=0; int max_it=1000;
  
  while( ( (t_up-t_down) > (1e3*DBL_MIN) ) & (it < max_it) ){ //anyway it is never reached, is there just to constraint maxit
    
    t_mid = (t_up+t_down)*0.5;
    
    ess_mid = cESS_log( ((t_ref/t_mid -1.)*ll) , lW);
    
    if( std::abs(ess_mid - thresh) <= eps ) return t_mid;
    
    if( sgn(ess_up - thresh) == sgn(ess_mid - thresh) ){
      t_up = t_mid;
      ess_up = ess_mid;
    }else{
      t_down = t_mid;
      ess_down = ess_mid;
    }
    
    ++it;
    
  }
  
  // return t_up;
  
  if(ess_down <= 5 ) return t_up;
  else{ 
    t_mid = (t_up+t_down)*0.5;
    ess_mid = cESS_log( ((t_ref/t_mid -1.)*ll) , lW);
    if( std::isfinite(ess_mid) && ess_mid!=0) return t_mid;
    else return t_up;
  }
  
}

double find_temp_cESS(vec lW, vec ll,double t_ref,double cess_ref,double thresh){
  //braketing
  // IMPORTANT: REMEMBER TO PASS TEMPERED LIKELIHOOD wrt T_REF AND LOG_WEIGHTS!

  int n = lW.n_elem;
  //make sure that the previous weights are normalized
  double sumlW = lW(0);
  for(int i=1;i<n;i++) sumlW = log_add(sumlW,lW(i));
  lW = lW - sumlW;

  
  double t_up = t_ref;
  double ess_up = cess_ref;
  int n_int = 100;
  double int_length = (t_up-1.0)/(double)n_int;
  double ess_down,t_down;
  
  int i=0;
  while(i<n_int){
    t_down = t_up - int_length;
    ess_down = cESS_log( ((t_ref/t_down)*ll - ll ), lW);
    
    if(sgn(ess_up - thresh) != sgn(ess_down - thresh)){
      std::cout<<" Found braketing in "<<t_up<<"( "<<ess_up<<" ) - "<<t_down<<"( "<<ess_down<<" ) ... ";
      return bisection_cESS(lW,ll,t_ref,t_up,t_down,ess_up,ess_down,thresh);
    }
    
    i++;
    t_up = t_down;
    ess_up = ess_down;
  }
  
  //last hope, the root is nowhere because in 1 we are still over thresh!
  if( (ess_down - thresh) >= 0 ) return 1.0;
  
  //if not...
  std::cout<<"Something went wrong int the bisection..."<<endl;
  return 0.0000001;  //will get the ESS to 0 and lead to complete degeneracy.
}

mat distMat(mat X){ //compute the distance matrix
  int r = X.n_rows;
  //int c = X.n_cols;
  
  mat Dx = zeros<mat>(r,r);
  
  for (int i = 0; i < r; i++) {
    for (int j = i; j < r; j++) {    
      Dx(i,j) = sqrt(sum(pow(X.row(i) - X.row(j),2)));
      if( i != j ) Dx(j,i) = Dx(i,j);
    }
  }
  
  return Dx;
} //distance matrix

mat matern(double ni,double sigma2,double rho,mat D){ //matern covariance function
  
  // should add checks for the parameters >0
  int s = D.n_rows;
  mat E(s,s);
  
  if(ni == 0.5){
    
    E = sigma2 * exp( -D/rho );
    
  }else{
    
    if(ni == 3./2.){
      
      double sqrt_3 = std::sqrt(3.);
      E = sigma2 * ( 1. + sqrt_3/rho * D ) % exp( -sqrt_3/rho * D );
      
    }else{
      
      if(ni == 5./2.){
        
        double sqrt_5 = std::sqrt(5.);
        E = sigma2 * ( 1. + ((sqrt_5*D)/rho) + ((5.*(D%D))/(3.*pow(rho,2))) ) % exp( -(sqrt_5*D)/rho );
        
      }else{
        
        for(int i=0;i<s;i++){
          for(int j=i;j<s;j++){
            E(i,j) = sigma2;
            if(i != j){
              E(i,j) = E(i,j)/(tgamma(ni)*pow(2,(ni-1))) * pow(sqrt(2*ni) * D(i,j)/rho,ni) * boost::math::cyl_bessel_k(ni,sqrt(2*ni) * D(i,j)/rho);
              E(j,i)= E(i,j);
            }}}
      }}}
  
  return E;
  
} //matern covariance function

double matern_point(double ni,double sigma2,double rho,double d){ //matern covariance function computed for distance d
  
  if( d == 0. ) return sigma2;
  // should add checks for the parameters >0
  if(ni == 0.5){
    
    return sigma2 * exp( -d/rho );
    
  }else{
    
    if(ni == 3./2.){
      
      double sqrt_3 = std::sqrt(3.);
      return sigma2 * ( 1 + sqrt_3/rho * d ) * exp( -sqrt_3/rho * d );
      
    }else{
      
      if(ni == 5./2.){
        
        double sqrt_5 = std::sqrt(5.);
        return sigma2 * ( 1. + sqrt_5/rho * d +5./(3.*pow(rho,2)) * std::pow(d,2) ) * exp( -sqrt_5/rho * d );
        
      }else{
        
        return sigma2/(tgamma(ni)*pow(2,(ni-1))) * pow(sqrt(2*ni) * d/rho,ni) * boost::math::cyl_bessel_k(ni,sqrt(2*ni) * d/rho);
        
      }}}
  
} //matern covariance function

// Misc functions for the wrapper
bool is_Symmetric( arma::mat &M , double tol){ //not for complex matrices?
  
  int d =  M.n_rows;
  
  for(int r=0; r<d; ++r) for(int c=r+1; c<d; ++c) if( ! (std::abs( M(r,c) - M(c,r) ) / std::abs(M(r,c))) < tol ) return false;
  
  // if it passes the test
  return true;
}


bool is_cov_matrix( arma::mat &M ){
  
  if( ! M.is_square() ) return false;
  if( ! is_Symmetric(M,1e-60) ) return false;

  arma::mat chol_M;
  if( ! chol(chol_M,M) ) return false;
  
  //if every test is passed
  return true;
}


/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 


/* ######################################################################## */
/* ######################################################################## */
/* ##########        Model functions for std models       ################# */
/* ######################################################################## */
/* ######################################################################## */



double lL(mat SC, int n,mat X,mat z,double sigma2_y,double phi_y){ //log likelihood
  
  int s = X.n_rows;
  
  mat Dxz = distMat(join_rows(X,z.t()));
  mat Exz = matern(ni_likelihood,sigma2_y,phi_y,Dxz);
  
  mat tmp; bool check = solve(tmp,Exz,SC);
  if(!check) return -DBL_MAX;  //if it fails because of phi_y -> Inf of Simga_y -> 0
  
  double log_det_Exz,sign;
  log_det(log_det_Exz,sign,Exz);
  
  double ll = -0.5*n*s*log(2*M_PI) -0.5*n*log_det_Exz -0.5*(n-1)*trace( tmp ); //should be tmp.t() but it's not necessary since we're dearling just with the trace...
  
  return ll;
  
  
} //log-likelihood

double t_lL(mat SC, vec y_bar, int n,mat X,mat z,double sigma2_y,double phi_y,double temp){ //tempered log likelihood (http://jonathantemplin.com/files/multivariate/mv11icpsr/mv11icpsr_lecture04.pdf p.34)
  
  int s = X.n_rows;
  
  mat Dxz = distMat(join_rows(X,z.t()));
  mat Exz = matern(ni_likelihood,sigma2_y,phi_y,Dxz);
  
  mat tmp; bool check = solve(tmp,Exz,SC + (y_bar * y_bar.t()));
  if(!check) return -std::numeric_limits<double>::infinity();  //if it fails because of phi_y -> Inf of Simga_y -> 0
  
  double log_det_Exz,sign;
  log_det(log_det_Exz,sign,Exz);
  
  double ll = (-0.5*n*s*log(2*M_PI) -0.5*n*log_det_Exz -0.5*n*trace( tmp ) )/temp; //should be tmp.t() but it's not necessary since we're dealing just with the trace...
  
  return ll;
  
  
} //tempered log-likelihood (better because less prone to numerical errors)

double lp(mat z,int p,double sigma2_y,double phi_y,double sigma2_z,double phi_z,double shape_p_y,double rate_p_y,double shape_s_y,double rate_s_y,double shape_p_z,double rate_p_z,double shape_s_z,double rate_s_z, mat Dx){
  
  int s = z.n_cols;
  
  // Priors
  double log_p_s_z,log_p_phi_z,log_p_z;
  
  if(p > 0){ 
    // Sigma_z (this could be with Dan Simpsons proposal)
    if(sigma2_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_s_z = (shape_s_z-1)*log(sigma2_z) -rate_s_z*sigma2_z + shape_s_z*log(rate_s_z) - boost::math::lgamma(shape_s_z);
    
    //phi_z
    if(phi_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_phi_z = (shape_p_z-1)*log(phi_z) -rate_p_z*phi_z + shape_p_z*log(rate_p_z) - boost::math::lgamma(shape_p_z);
    
    
    // Z|sigma_z,phi_z
    mat Ex = matern(ni_prior,sigma2_z,phi_z,Dx);
    mat iEx; bool check = inv_sympd(iEx,Ex);
    if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0
    
    log_p_z = 0.;
    double log_det_Ex,sign;
    log_det(log_det_Ex,sign,Ex);
    
    for(int i=0; i<p; ++i){
      log_p_z += -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar( (z.row(i)*iEx)*z.row(i).t() );
    }
    
  }else{
    log_p_s_z = log_p_phi_z = log_p_z = 0.; //otherwise set to a constant so that the it can continue without taking Z into account
  }
  
  // phi_y
  if(phi_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_phi_y = (shape_p_y-1)*log(phi_y) -rate_p_y*phi_y + shape_p_y*log(rate_p_y) - boost::math::lgamma(shape_p_y);
  
  // sigma 2 y
  if(sigma2_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_s_y = (shape_s_y-1)*log(sigma2_y) -rate_s_y*sigma2_y + shape_s_y*log(rate_s_y) - boost::math::lgamma(shape_s_y);
  
  // Joint prior
  double lp = ( log_p_s_z + log_p_phi_z + log_p_z + log_p_phi_y + log_p_s_y);
  
  return lp;
} //log prior

double t_lP(mat SC,vec y_bar,int n,mat X,mat z,int p,double sigma2_y,double phi_y,double sigma_z,double phi_z,double temp,double shape_p_y,double rate_p_y,double shape_s_y,double rate_s_y,double shape_p_z,double rate_p_z,double shape_s_z,double rate_s_z,mat Dx){ //log Posterior
  
  int s = z.n_cols;
  
  double log_prior = lp(z,p,sigma2_y,phi_y,sigma_z,phi_z,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
  
  //likelihood
  double t_log_likelihood = t_lL(SC,y_bar,n,X,z,sigma2_y,phi_y,temp);
  
  double log_posterior = ( log_prior + t_log_likelihood );
  
  return log_posterior;
  
} //(tempered) log Posterior


// for the time being let every Z_i is independent from one another in the prior AND THE CROSS-COVARIANCE MATRIX/FUNCTION BEING DIAGONAL, hence Z IS INDEP EVEN IN THE PROPOSAL!!
// alternatively Z_(i+1) could depend on [X,Z_(1:i)].
mat MV_elliptical_ss(mat Z,double &ll,mat Sigma,mat SC,vec y_bar,int n,mat X,double sigma2_y,double phi_y,double temp, double &count){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  
  //Choose the Ellipse
  mat nu(p,s); mat z_next(p,s);
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found;
  
  mat chol_Sigma;
  bool check = chol(chol_Sigma,Sigma); 
  if(!check){
    // ll = -std::numeric_limits<double>::infinity();
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  
  // Sample the new ellipse
  omp_set_lock(&RNGlock); // ************************ lock

  for(int i=0; i<p; ++i){
    nu.row(i) = normal_01(s).t() * chol_Sigma;
  }
  
  //ll threshold
  threshold = ll + log(random_01(rng));
  
  //Draw the first proposal, also defining initial brakets
  theta = random_01(rng)*2*M_PI;
  theta_min = theta - 2*M_PI;
  theta_max = theta;
  delta = theta_max-theta_min;
  
  omp_unset_lock(&RNGlock); // ************************ lock

  found = false;
  
  while(!found){

    count++;
    
    z_next = Z*cos(theta) + nu*sin(theta);
    
    // -- log Likelihood
    ll_next = t_lL(SC,y_bar,n,X,z_next,sigma2_y,phi_y,temp);

    if( !std::isfinite(ll_next) ){
      //Z and ll unchanged
      found = true;
    } 

    if(!found){ // to avoid comparisons with NAs
    
      if( ll_next >= threshold ){
        
        Z = z_next;
        ll = ll_next; //this should update even the external ll
        found = true;
        
      }else{
        
        if(theta < 0){theta_min = theta;}else{theta_max = theta;}
        
        delta = theta_max-theta_min;

        omp_set_lock(&RNGlock); // ************************ lock
          theta = random_01(rng)*(theta_max-theta_min)+theta_min;
        omp_unset_lock(&RNGlock); // ************************ lock
      }
      
      if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
        break;
      }
    }
    
  } 
  
  return Z;
  
} //is this even correct???

mat elliptical_ss(mat Z,double &ll,mat Sigma,mat SC,vec y_bar,int n,mat X,double sigma2_y,double phi_y,double &count, double temp=1){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  
  //Choose the Ellipse(s)
  mat nu(p,s);
  mat z_next = Z;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found;
  mat chol_Sigma;
  bool check = chol(chol_Sigma,Sigma); 
  if(!check){
    // ll = -std::numeric_limits<double>::infinity();
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  
  
  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock

  for(int i=0; i<p; ++i){
    nu.row(i) = normal_01(s).t() * chol_Sigma;
  }

  omp_unset_lock(&RNGlock); // ************************ lock


  // Then for each Z, run Ell SS

  for(int i=0; i<p; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above

    omp_set_lock(&RNGlock); // ************************ lock

    //ll threshold
    threshold = ll + log(random_01(rng));  

    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;
    delta = theta_max-theta_min;
    
    omp_unset_lock(&RNGlock); // ************************ lock

    found = false;
    
    while(!found){

      count++;
      
      z_next.row(i) = Z.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = t_lL(SC,y_bar,n,X,z_next,sigma2_y,phi_y,temp);

      if( !std::isfinite(ll_next) ){
        //Z and ll unchanged
        found = true;
      } 

      if(!found){ // to avoid comparisons with NAs
      
        if( ll_next >= threshold ){
          
          Z.row(i) = z_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;

          omp_set_lock(&RNGlock); // ************************ lock
            theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
  
  }

  return Z;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}


/* #########################################################################
############################################################################
#######################    Dim Exp  Main     ###############################
############################################################################
############################################################################ */


/* #########################################################################
####################    TransDim Dim Exp MCMC     ##########################
############################################################################ */

Rcpp::List DE_MCMC(int n_iter, mat X, mat SampleCov, vec y_bar, int n, double target_ar , int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                  double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
  
{
  
  omp_init_lock(&RNGlock);  

  rng.seed(time(0));
  
  vec p(n_iter);
  int s = X.n_rows;
  int d = X.n_cols;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  // Prior for p
  int max_p = 3*d; // According to Perrins we could go down to 2* ... right?
  int min_p = 1; // 0 = baseline model, but the RJ step is weird if I allow 0
  boost::random::uniform_int_distribution<> unif_p_prior(min_p, max_p); //We could make it different than the uniform


  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper-Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  cube Z = zeros<cube>(max_p,s,n_iter);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)
  // the dim is set to max_p, as I cannot exceed that and it's more memory efficient than allocating a new mat each time
  rowvec tmp_z;
  
  vec lsigma2_y(n_iter); vec lp_y(n_iter); vec lsigma2_z(n_iter);vec lp_z(n_iter);
  mat theta(n_iter,4); mat theta_new(n_iter,4);
  
  int p_new; vec tmp_p(2);
  double tmp_sz, tmp_pz;

  vec ll_curr(n_iter); ll_curr.fill(-DBL_MAX);
  double ll_new;
  
  vec lprior_curr(n_iter);
  double lprior_new;
  
  vec lw = zeros(n_iter); lw.fill(0.); // log
  vec lW(n_iter); lW.fill(log(1./n_iter)); //log normalized
  vec W(n_iter); W.fill(1./n_iter);//normalized
  
  double ess;
  vec rand(n_iter); vec accepted(n_iter);
  double acc_rate;
  double trans_acc_rate;  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_iter);
  double sum_lw;
  
  
  mat tmpMat(n_iter,s);
  vec tmpVec(n_iter);
  mat tmp_Ex(s,s);
  mat c_Ex(s,s);
  double sign,log_det_Ex;

  //MCMC
  mat Sigma_prop = eye<mat>(4,4);
  mat chol_Sigma_prop = eye<mat>(4,4);
  mat old_Sigma_prop = eye<mat>(4,4);
  mat Sigma_prop_0 = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0.;
  double old_sd_s = 0.;
  
  vec tmp_vec;

    vec essCount= zeros<vec>(n_iter);

  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... ";
  
  mat Dx = distMat(X);
  

  // p (sort of model index)
  p(0) = as_scalar(random_prior_p(1,unif_p_prior)); //p(0)=2; //DEBUG

  // sigma_z & phi_z
  lsigma2_z(0) = as_scalar(log(random_gamma(1,sigma_z_prior))); 
  lp_z(0) = as_scalar(log(random_gamma(1,phi_z_prior))); 

  // Z
  tmp_Ex = matern(ni_prior,exp(lsigma2_z(0)),exp(lp_z(0)),Dx);
  c_Ex = chol(tmp_Ex);

  mat tmp_Z= zeros<mat>(max_p,s) ;

  for(int k=0; k<p(0); ++k){
  Z.slice(0).row(k) = normal_01(s).t() * c_Ex ; // independent for now in the prior
  }
    
  
  // sigma and phi_y
  lsigma2_y(0) = as_scalar(log(random_gamma(1,sigma_y_prior)));
  lp_y(0) = as_scalar(log(random_gamma(1,phi_y_prior))); 
  //note that here the convention is shape & scale!!!

  if(std::isnan(lsigma2_y(0)) || !std::isfinite(lsigma2_y(0)) || 
     std::isnan(lp_y(0))      || !std::isfinite(lp_y(0))      ||
     std::isnan(lsigma2_z(0)) || !std::isfinite(lsigma2_z(0)) ||
     std::isnan(lp_z(0))      || !std::isfinite(lp_z(0))       ){

          std::cout << " Error in initializing parameters from the Prior, check input!" << std::endl;
          return Rcpp::List::create( n_iter );
  } 

  Sigma_prop(0,0) = Sigma_prop_0(0,0) = 0.0005; //boost::math::trigamma(shape_s); // variance of the prior
  Sigma_prop(1,1) = Sigma_prop_0(1,1) = 0.0005; //boost::math::trigamma(shape_p);
  Sigma_prop(2,2) = Sigma_prop_0(2,2) = 0.0005; //boost::math::trigamma(shape_s);
  Sigma_prop(3,3) = Sigma_prop_0(3,3) = 0.0005; //boost::math::trigamma(shape_p);
  
  chol(chol_Sigma_prop,Sigma_prop_0); // Compute the first cholewski decomp of the proposal matrix

  //compute the starting posterior value
  lprior_curr(0) = lp(Z.slice(0),p(0),exp(lsigma2_y(0)),exp(lp_y(0)),exp(lsigma2_z(0)),exp(lp_z(0)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
  ll_curr(0) = lL(SampleCov,n,X,Z.slice(0),exp(lsigma2_y(0)),exp(lp_y(0)));

  std::cout << "Done!" << std::endl << "Starting MCMC ... " << endl;

  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  double count = 0.; double trans_count=0.;
  vec acc_last_100 = zeros<vec>(100);

  int adapt_threshold = std::max(100.,n_iter/100.); cout << "Start adapting at " << adapt_threshold << "Accepted values" << endl;

  theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
  vec mean_theta(4);
  mat Sigma_theta = eye<mat>(4,4);
  // here on try to work only on theta.
  
  for(int i=1; i<n_iter; ++i)
  {
    
    /******************************************
    ********* MCMC moves **********************
    ******************************************/

    /*************************************
    ***** Update for log-parameters  *****      lphi_y , lsigma_y, lsigma2_z, lphi_z | Z (rw proposals)
    *************************************/

    //Set tuning parameters for the MCMC move

    if(count == adapt_threshold){                 // Initialize adaptation
      std::cout << "THERE WE GO! ADAPT!" << endl;
      mean_theta = mean(theta.rows(0,i-2.)).t();
      Sigma_theta = cov(theta.rows(0,i-1.));
    } 


    if(count > adapt_threshold){       // continue adaptation TODO there's a problem here coz Sigma_prop is not always symmetric :/

      mean_theta = ( (i-3.) * mean_theta + theta.row(i-2.).t() ) / (i-2.);
      Sigma_theta = ( (i-3.)/(i-2.) * Sigma_theta ) + ( ((theta.row(i-1.)).t() - mean_theta) * ((theta.row(i-1.)).t() - mean_theta).t() ) / (i-1.);

      if( !chol(tmp_mat,Sigma_theta) ){ Sigma_theta = cov(theta.rows(0,i-1.)); }//else{ cout << " ! ";}

      tmp_mat = Sigma_prop + 10.* pow(i,(-1.0001)) * ( as_scalar(mean(acc_last_100)) - target_ar) * ( Sigma_theta + Sigma_prop_0 )/2.;

      if( chol(chol_Sigma_prop,tmp_mat) ){
          Sigma_prop = tmp_mat; 
      }else{ 
      //          cout << " ? ";
          chol(chol_Sigma_prop,Sigma_prop);  // Sigma_prop unchanged from previous iteration. (it get reset if chol fails before so I need to recompute)
      } 

    }

    // End adaptation

    theta.row(i) = theta.row(i-1); // set for failure as default
    ll_curr(i) = ll_curr(i-1);
    lprior_curr(i) = lprior_curr(i-1);

    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      tmp_vec = normal_01(4);

      if( p(i-1) == 0 ){ // if p == 0 p_z and s_z stay somehow latent
          tmp_vec(2) = 0.;
          tmp_vec(3) = 0.;

          tmp_vec.subvec(0,1) = chol_Sigma_prop.submat(0,0,1,1) * tmp_vec.subvec(0,1); 

      }else{

          tmp_vec =chol_Sigma_prop * tmp_vec; 
      }

      theta_new = theta.row(i).t() + tmp_vec;
      
      // Check boundaries
      acc_rate = 0.;
      if( std::isnan(theta_new(0)) || !std::isfinite(exp(theta_new(0))) ||
          std::isnan(theta_new(1)) || !std::isfinite(exp(theta_new(1))) ||
          std::isnan(theta_new(2)) || !std::isfinite(exp(theta_new(2))) ||
          std::isnan(theta_new(3)) || !std::isfinite(exp(theta_new(3))) ){ 
        // cout<< " Proposed invalid value! ";
        acc_rate = -DBL_MAX;

      }else{

      //**** Joint Acceptance rate ****

      // Posterior ratio
      ll_new = lL(SampleCov,n,X,Z.slice(i-1),exp(theta_new(0)),exp(theta_new(1)));

      lprior_new = lp(Z.slice(i-1),p(i-1),exp(theta_new(0)),exp(theta_new(1)),exp(theta_new(2)),exp(theta_new(3)),
                      shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      acc_rate = (ll_new + lprior_new) - (ll_curr(i) + lprior_curr(i));

      if( std::isnan(acc_rate) || !std::isfinite(acc_rate) ){ acc_rate = -DBL_MAX; }


      //proposal correction (the proposal on log-sigma2 is symmetric)

      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..

      acc_rate += (theta_new(0) - theta(i-1,0)) + (theta_new(1) - theta(i-1,1)) + (theta_new(2) - theta(i-1,2)) + (theta_new(3) - theta(i-1,3));

      } // end if parameters are in range


      // ***** Finally Updating *****
      if( log( random_01(rng) ) < acc_rate){

        theta.row(i) = theta_new.t();

        ll_curr(i) = ll_new;
        lprior_curr(i) = lprior_new;

        ++count;
        acc_last_100(i % 100) = 1;


      }else{

        // theta.row(i) = theta.row(i-1); // COMMENTED AS IT'S LAREADY DONE AT THE START, same for ll_curr and lprior_curr
        acc_last_100(i % 100) = 0;

      }

      // Z and p are not changed here
      
    } //end multiple MCMC steps
    
    
    //******************************************
    //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
    //******************************************

    Z.slice(i).rows(0,p(i-1)-1) = Z.slice(i-1).rows(0,p(i-1)-1); // set the same as of now
    
    for(int jj=0;jj<J;jj++){ // for more then one step...
      
      if( p(i-1) > 0 ){

        Z.slice(i).rows(0,p(i-1)-1) = elliptical_ss( Z.slice(i).rows(0,p(i-1)-1) , ll_curr(i), matern(ni_prior,exp(theta(i,2)),exp(theta(i,3)),Dx) ,SampleCov,y_bar,n,X,exp(theta(i,0)),exp(theta(i,1)),essCount(i));

        // Note that ll_curr is updated by passing it by reference (could do it for Z as well...)
        // lprior needs to be updated though
        lprior_curr(i) = lp(Z.slice(i),p(i-1),exp(theta(i,0)),exp(theta(i,1)),exp(theta(i,2)),exp(theta(i,3)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      } //if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same

    } // loop J times EllSliceSsamp
    
    essCount(i) = ((essCount(i) / J) / p(i-1));

    //**************************************************
    // ******************* TRANSDIMENTIONAL MOVE! ******    update for p (and possibly Z)
    //**************************************************

    // TODO check for the p=0 move and eventually implement it correctly

    // Propose a new dimension (i.e. p | Z, phi, sigma, lsigma2_z)

    tmp_p = p_proposal(p(i-1),min_p,max_p);

    tmp_Z = Z.slice(i); // current parameters
    tmp_sz = theta(i,2);
    tmp_pz = theta(i,3);

    // is it in higher or lower dimension?
    // in case of higher propose the parameters (Z) accordingly
    // Compute the probability of the move from lower to higher dim and reverse in opposite case

    if( (tmp_p(0) > 0) && (p(i-1) > 0) ){ // ****** if both are greater than 0 no need to care for the spare phi_z and sigma_z *****

    if( tmp_p(0) > p(i-1) ){ // can only be greater by 1 for now

      // sample new dimension for Zs, compute their probability, add them to the current parameter
      tmp_Ex = matern(ni_prior,exp(tmp_sz),exp(tmp_pz),Dx);
      log_det(log_det_Ex,sign,tmp_Ex);

      tmp_z = normal_01(s).t() * chol(tmp_Ex); //still taking only X into account and not the already-there Z
      tmp_Z.row(tmp_p(0)-1) = tmp_z;

      trans_acc_rate = log(tmp_p(1)) - //TODO CHECK why the hell am I re-using tmp_p(1) for this?
      ( -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar(tmp_z*solve(tmp_Ex,tmp_z.t())) ); //term relative to the sampling distribution

      // compute acceptance prob for the move

      ll_new = lL(SampleCov,n,X,tmp_Z,exp(theta(i,0)),exp(theta(i,1)));
      lprior_new = lp(tmp_Z,tmp_p(0),exp(theta(i,0)),exp(theta(i,1)),exp(tmp_sz),exp(tmp_pz),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      trans_acc_rate += (ll_new + lprior_new) - (ll_curr(i) + lprior_curr(i)); // posterior log-ratio

      trans_acc_rate += 0; // determinant of the Jacobian *should* be 1 TODO CHECK

    }

    if(tmp_p(0) < p(i-1)){

      // shed the last dimension for Z, compute their probability
      tmp_Ex = matern(ni_prior,exp(tmp_sz),exp(tmp_pz),Dx);
      log_det(log_det_Ex,sign,tmp_Ex);

      tmp_z = tmp_Z.row(p(i-1)-1);   // could use tmp_Z.row(tmp_p(0)), maybe even better for general proposals ...
      tmp_Z.row(p(i-1)-1) = zeros<rowvec>(s); //erase the last dim

      trans_acc_rate = -log(tmp_p(1)) -  // with minus sign cause I am considering the reverse as of now
      ( -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar(tmp_z*solve(tmp_Ex,tmp_z.t())) );

      ll_new = lL(SampleCov,n,X,tmp_Z,exp(theta(i,0)),exp(theta(i,1)));
      lprior_new = lp(tmp_Z,tmp_p(0),exp(theta(i,0)),exp(theta(i,1)),exp(tmp_sz),exp(tmp_pz),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      trans_acc_rate += (ll_curr(i) + lprior_curr(i)) - (ll_new + lprior_new) ; // posterior log-ratio considered transitioning from small to big

      trans_acc_rate += 0;// determinant of the Jacobian *should* be 1 TODO CHECK

      trans_acc_rate = -trans_acc_rate; // finally reverse the whole acc ratio

    }

    if(tmp_p(0) == p(i-1)){ //shoud not be possible anyway...
      trans_acc_rate = -DBL_MAX; //reject TODO CHECK, is this correct? I could simply remove this possibility from the proposal..
    // Conceptually this does not impact anyway as tmp_Z and Z are still equal so we could also accept, but that is a wasted copy
    }


    }else{ // if one of them is the baseline model **************

    if( tmp_p(0) > p(i-1) ){ // so p(j) == 0 and tmp_p >=  1 ***
    // *** Need to sample the extra phi_z and sigma_z

      tmp_sz = as_scalar(log( random_gamma(1,sigma_z_prior)) ); //tmp_sz(j) = log(1.) + as_scalar(normal_01(1)/500.); //DEBUG
      tmp_pz = as_scalar(log( random_gamma(1,phi_z_prior)) ); //tmp_pz(j) = log(0.5) + as_scalar(normal_01(1)/500.); //DEBUG

      // sample new dimension for Zs, compute their probability, add them to the current parameter
      tmp_Ex = matern(ni_prior,exp(tmp_sz),exp(tmp_pz),Dx);
      log_det(log_det_Ex,sign,tmp_Ex);

      tmp_z = normal_01(s).t() *chol(tmp_Ex); //still taking only X into account and not the already-there Z
      tmp_Z.row(0) = tmp_z;

      trans_acc_rate = log(tmp_p(1)) - // proposal for p
      ( -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar(tmp_z*solve(tmp_Ex,tmp_z.t())) ) -
      (shape_s_z-1)*log(tmp_sz) -rate_s_z*tmp_sz + shape_s_z*log(rate_s_z) - boost::math::lgamma(shape_s_z)-
      (shape_p_z-1)*log(tmp_pz) -rate_p_z*tmp_pz + shape_p_z*log(rate_p_z) - boost::math::lgamma(shape_p_z); //last 3 term relative to the sampling distribution

      // compute posterior prob ratio for the move

      ll_new = lL(SampleCov,n,X,tmp_Z,exp(theta(i,0)),exp(theta(i,0)));
      lprior_new = lp(tmp_Z,tmp_p(0),exp(theta(i,0)),exp(theta(i,1)),exp(tmp_sz),exp(tmp_pz),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      trans_acc_rate += (ll_new + lprior_new) - (ll_curr(i) + lprior_curr(i)); // posterior log-ratio

      trans_acc_rate += 0; // determinant of the Jacobian *should* be 1 TODO CHECK

    }

    if(tmp_p(0) < p(i-1)){ //in this case p(j) >= 1 and tmp_p == 0 ****

      // *** the extra phi_z and sigma_z are shedded and considered as sampled from ... prior distr atm
      // shed the last dimension for Z, compute their probability
      tmp_Ex = matern(ni_prior,exp(tmp_sz),exp(tmp_pz),Dx);
      log_det(log_det_Ex,sign,tmp_Ex);

      tmp_z = tmp_Z.row(0);
      tmp_Z.row(0) = zeros<rowvec>(s); //erase the last dim

      trans_acc_rate = -log(tmp_p(1)) -  // with minus sign cause I am considering the reverse as of now
      ( -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar(tmp_z*solve(tmp_Ex,tmp_z.t())) ) -
      (shape_s_z-1)*log(tmp_sz) -rate_s_z*tmp_sz + shape_s_z*log(rate_s_z) - boost::math::lgamma(shape_s_z) -
      (shape_p_z-1)*log(tmp_pz) -rate_p_z*tmp_pz + shape_p_z*log(rate_p_z) - boost::math::lgamma(shape_p_z); //last 3 term relative to the sampling distribution


      ll_new = lL(SampleCov,n,X,tmp_Z,exp(theta(i,0)),exp(theta(i,0)));
      lprior_new = lp(tmp_Z,tmp_p(0),exp(theta(i,0)),exp(theta(i,1)),exp(tmp_sz),exp(tmp_pz),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);

      trans_acc_rate += (ll_curr(i) + lprior_curr(i)) - (ll_new + lprior_new) ; // posterior log-ratio considered transitioning from small to big

      trans_acc_rate += 0;// determinant of the Jacobian *should* be 1 TODO CHECK

      trans_acc_rate = -trans_acc_rate; // finally reverse the whole acc ratio

    }

    if(tmp_p(0) == p(i-1)){ // both equal to 0, even less possible. **
      trans_acc_rate = -DBL_MAX; //reject TODO CHECK, is this correct? I could simply remove this possibility from the proposal..
    // Conceptually this does not impact anyway as tmp_Z and Z are still equal so we could also accept, but that is a wasted copy
    }

    } // end if one is 0 do, else do other


    // So now this proposal for this particle has been computed. nothing left to do, going onto the next.


    // Update the accepted moves... *****

    if( log( random_01(rng) ) < trans_acc_rate){

      Z.slice(i) = tmp_Z;
      p(i) = tmp_p(0); //(0,idx)

      theta(i,2) = tmp_sz;
      theta(i,3) = tmp_pz;

      ll_curr(i) = ll_new;
      lprior_curr(i) = lprior_new;

      ++trans_count;


    }else{

      p(i) = p(i-1);
      // rest stays the same

    }      
  
    //  ********* END full-MCMC MOVE
  
  
  
    // Visual Output per iteration

    if( ((i+1) % 100) == 0 ){

      cout << "Iteration #" << i+1 << endl;
      cout << "Param acceptance rate = " << (double)count/i << " --- Transd. acceptance rate = " << (double)trans_count/i << endl;
      cout << "Partial Estimates: Sigma_y ~ " << as_scalar( mean( exp(theta.submat(0,0,i,0)) )) << " --- Phi_y ~ " << as_scalar( mean( exp(theta.submat(0,1,i,1)) )) << endl;
      cout << "Partial average extra-dimensions ( Z ) is " << mean(p.subvec(0,i)) << endl;
      cout << "Proposal matrix atm is:"<<endl<< Sigma_prop << endl<<endl;

    }

  }  // end MCMC for loop
    
  std::cout<<"MCMC Done!"<<std::endl;
    
  uword burnin = floor(n_iter/2) ;

  // re-expand theta for clarity
  lsigma2_y = theta.submat(burnin,0,n_iter-1,0);
  lp_y = theta.submat(burnin,1,n_iter-1,1);
  lsigma2_z = theta.submat(burnin,2,n_iter-1,2);
  lp_z = theta.submat(burnin,2,n_iter-1,2);


  // Resize p, Z and ll, lp as well
  Z.shed_slices(0,burnin-1);
  p.shed_rows(0,burnin-1);
  ll_curr.shed_rows(0,burnin-1);
  lprior_curr.shed_rows(0,burnin-1);
  
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
      // note that here Z and all the params have already been resized!
      
  // cube post_Dxz(s,s,n_iter-burnin);
  // cube post_Exz(s,s,n_iter-burnin);
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_iter-burnin; ++j){
  //   post_Dxz.slice(j) = distMat(join_rows(X,trans(Z.slice(j))));
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j));
  // }
  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<(n_iter-burnin); ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,trans(Z.slice(j))))); 
  }
  post_mean_Exz = post_mean_Exz/(double)(n_iter-burnin);

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
									  Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=p,Rcpp::Named("Z")=Z,
                                      Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,
                                      Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("log_phi_z")=lp_z,
                                      Rcpp::Named("ll_final")=ll_curr,Rcpp::Named("lprior_final")=lprior_curr,
                                      Rcpp::Named("mean_ess_count")=mean(essCount)
                                      );
  
  return out;
  
  // Maybe in this case (MCMC) return just the estimations? just more burnin won't do as I need a long history to be able to estimate the params correctly. A short list of last values won't let me estimate correctly at all.
  // If I'd return just estimates I could do it for every p and then return the posterior for p (which is discrete so very low mem consumption)

}



/* #########################################################################
########################  No Expansion, baseline main   ####################
############################################################################ */

Rcpp::List baseline_SMC(int n_part, mat X, mat SampleCov, vec y_bar, int n, double factor_temp ,double thresh, int det_schedule, int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y)
  
{
  
  omp_init_lock(&RNGlock);  

  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part);
  mat theta(n_part,2); mat theta_new(n_part,2);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  // cube post_Dxz(s,s,n_part);
  // cube post_Exz(s,s,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  
  mat tmpMat(n_part,s);
  vec tmpVec(n_part);
  mat tmp_Ex(s,s);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(2,2);
  mat chol_Sigma_prop = eye<mat>(2,2);
  mat old_Sigma_prop = eye<mat>(2,2);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);
  
  // Dummy zeta vars
  mat dummy_z = zeros<mat>(1,s);
  double dummy_p_z = 1.; double dummy_s_z = 1.;
  double shape_p_z = 1.; double rate_p_z = 1.;double shape_s_z = 1.;double rate_s_z = 1.;
  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... ";
  
  mat Dx = distMat(X);
  
  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  
  mat Sigma_prop_0 = old_Sigma_prop;
  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = lp(dummy_z,0,exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
    ll_curr(j) = t_lL(SampleCov,y_bar,n,X,dummy_z,exp(lsigma2_y(j)),exp(lp_y(j)),temp(i));
  }
  
  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem) temp.resize(2*temp.n_elem);
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp(lw,ll_tmp,temp(i-1),ess,ess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }

    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviour!";
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 

    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    /*
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 
    
    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** ESS ********
    ess = ESS_log(lw);
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0.);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = multi_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      lp_y = lp_y.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    // TODO now could be done better...
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(1,0) = Sigma_prop(0,1);
    
    Sigma_prop = factor_sd * Sigma_prop;
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
  
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
  
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;

    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      Sigma_prop = old_Sigma_prop;
      chol(chol_Sigma_prop,Sigma_prop);
    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*2);
      tmp_mat.resize(n_part,2);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      theta = join_horiz(lsigma2_y,lp_y);
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = t_lL(SampleCov,y_bar,n,X,dummy_z,exp(theta_new(j,0)),exp(theta_new(j,1)),temp(i));
        lprior_new(j) = lp(dummy_z,0,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
        
      }  
      
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }  
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****   would it be faster with a for loop parallelized?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    
    //  ********* END full-MCMC MOVE (no move on the Zs now as there are none)
    
    
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_part; ++j){
  //   post_Dxz.slice(j) = Dx;
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j));
  // }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }
  
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);
  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,trans(dummy_z)))); 
  }
  post_mean_Exz = post_mean_Exz/(double)n_part;

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
  												  Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=0,Rcpp::Named("Z")=dummy_z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=1.,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=1., Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp);
  return out;
  
}


/* #########################################################################
####################    Dim Exp  SMC fixed p     ###########################
############################################################################ */

Rcpp::List DE_SMC(int n_part, mat X, mat SampleCov, vec y_bar, int n,int p, double factor_temp ,double thresh, int det_schedule, int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                  double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper_Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  
  cube Z = zeros<cube>(p,s,n_part);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part); vec lsigma2_z(n_part);vec lp_z(n_part);
  mat theta(n_part,4); mat theta_new(n_part,4);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  
  // cube post_Dxz(s,s,n_part);
  // cube post_Exz(s,s,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  vec essCount(n);

  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  
  mat tmpMat(n_part,s);
  vec tmpVec(n_part);
  mat tmp_Ex(s,s); mat c_Ex(s,s);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(4,4);;
  mat chol_Sigma_prop = eye<mat>(4,4);;
  mat old_Sigma_prop = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);
  
  bool check;
  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************

    // silly cout
  std::cout << "Threshold for resampling: ESS <= " << thresh << std::endl;
  
  // Sim from the prior
  std::cout << "Sim from the prior ... ";
  
  mat Dx = distMat(X);
  
  // DISCLAMER: IF p==0 Z, sigma_z and phi_z are useless, but as they do not impact ll and lp I will keep them despite the computation loss
  // TODO REMOVE IT, especially the Zs as they impact A LOT the computing time (as for now, I will just remove the EllSliceSamp)
  
  // sigma_z & phi_z
  lsigma2_z = log(random_gamma(n_part,sigma_z_prior)); //lsigma2_z = log(1.) + normal_01(n_part)/500.; //DEBUG
  lp_z = log(random_gamma(n_part,phi_z_prior)); //lp_z = log(0.5) + normal_01(n_part)/500.; //DEBUG
  
  // Z
  cube tmp_Z= zeros<cube>(p,s,n_part) ;
    
  #pragma omp parallel for private(tmp_Ex,c_Ex)
  for(int j=0; j<n_part; ++j){

    tmp_Ex = matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx);
    check = chol(c_Ex,tmp_Ex);

    if(check){
        for(int k=0; k<p; ++k){
    
          omp_set_lock(&RNGlock); // ************************ lock
            Z.slice(j).row(k) = normal_01(s).t(); // chol(tmp_Ex) * random std normals
          omp_unset_lock(&RNGlock); // ************************ lock
    
        }
    
        Z.slice(j) = Z.slice(j) * c_Ex; 
    }
  }

  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
    if(std::isnan(lsigma2_z(j)) || !std::isfinite(lsigma2_z(j))) lsigma2_z(j)=-DBL_MAX;
    if(std::isnan(lp_z(j)) || !std::isfinite(lp_z(j))) lp_z(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  old_Sigma_prop(2,2) = w_var(lsigma2_z,W);
  old_Sigma_prop(3,3) = w_var(lp_z,W);
  

  mat Sigma_prop_0 = old_Sigma_prop;
  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
    ll_curr(j) = t_lL(SampleCov,y_bar,n,X,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),temp(i));
  }
  
  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem){ 
      temp.resize(2*temp.n_elem);
      essCount.resize(2*essCount.n_elem);
    }
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }
    
    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    /*
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)


    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 
    
    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = multi_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      
      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    // TODO now could be done better...
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(2,2) = w_var(lsigma2_z,W);
    Sigma_prop(3,3) = w_var(lp_z,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(0,2) = w_cov(lsigma2_y,lsigma2_z,W);
    Sigma_prop(0,3) = w_cov(lsigma2_y,lp_z,W);
    
    Sigma_prop(1,2) = w_cov(lp_y,lsigma2_z,W);
    Sigma_prop(1,3) = w_cov(lp_y,lp_z,W);
    
    Sigma_prop(2,3) = w_cov(lsigma2_z,lp_z,W);
    
    Sigma_prop(1,0) = Sigma_prop(0,1);
    Sigma_prop(2,0) = Sigma_prop(0,2);
    Sigma_prop(3,0) = Sigma_prop(0,3);
    Sigma_prop(2,1) = Sigma_prop(1,2);
    Sigma_prop(3,1) = Sigma_prop(1,3);
    Sigma_prop(3,2) = Sigma_prop(2,3);
    
    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;


    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      Sigma_prop = old_Sigma_prop;
      chol(chol_Sigma_prop,Sigma_prop);
    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*4);
      tmp_mat.resize(n_part,4);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = t_lL(SampleCov,y_bar,n,X,Z.slice(j),exp(theta_new(j,0)),exp(theta_new(j,1)),temp(i));
        lprior_new(j) = lp(Z.slice(j),p,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
      }
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)) + (theta_new(j,2) - theta(j,2)) + (theta_new(j,3) - theta(j,3)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      lsigma2_z = theta.col(2);
      lp_z = theta.col(3);
      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);
      
      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    
    if(p>0){
      //******************************************
      //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
      //******************************************

      essCount(i) = 0;
      
      for(int jj=0;jj<J;jj++){ // for more then one step...
        
        std::cout << "Ell_SS step #" <<jj+1 << " ... ";  
        
        #pragma omp parallel for
        for(int j=0; j<n_part; ++j){ //every particle
          
          Z.slice(j) = elliptical_ss( Z.slice(j) , ll_curr(j), matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx) ,SampleCov,y_bar,n,X,exp(lsigma2_y(j)),exp(lp_y(j)),essCount(i),temp(i));
          
          // Note that ll_curr is updated by passing it by reference (I could actually do the same for Z)
          // lprior needs to be updated though 
          lprior_curr(j) = lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx);
          
        } // loop for every particle
      } // loop J times EllSliceSsamp
      

      essCount(i) = ((essCount(i) / n_part) / J )/ p ;

      std::cout << "(avg#steps " << essCount(i) << ") -- Done!" << endl;
      
      // Note there is no need to zero out the rest as for now the dimension is fixed
      
    } // if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same
    
    
    //  ********* END full-MCMC MOVE
    
    
    
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_part; ++j){
  //   post_Dxz.slice(j) = distMat(join_rows(X,trans(Z.slice(j))));
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j)); //cant I just translate X at the start?
  // }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
   
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);
  essCount.resize(i);

  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,trans(Z.slice(j))))); 
  }
  post_mean_Exz = post_mean_Exz/(double)n_part;

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
  												  Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=p,Rcpp::Named("Z")=Z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=lp_z, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp, Rcpp::Named("avg_ess_cout") = mean(essCount));
  return out;
  
}

/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 


/* ######################################################################## */
/* ######################################################################## */
/* ########## Model functions again for predictive models ################# */
/* ######################################################################## */
/* ######################################################################## */



/****************************************************************************/
/***************** lL with integration on y_star ****************************/
/****************************************************************************/

// integrated liklihood, computations still of order O(s^2)
double predictive_int_lL(const mat &y, int n, const mat &X, const mat &X_star, const mat &z_star, double sigma2_y, double phi_y, double sigma2_z, double phi_z, const mat &Dx_star){ //log likelihood ( z is passed as a/multiple row/s from a matrix)
  
  int s = y.n_cols;
  int s_star = z_star.n_cols;
  
  // compute the necessary matrices
  // mat Ex = matern(nu_z,sigma2_z,phi_z,Dx);
  mat Rx_star = matern(nu_z,1.,phi_z,Dx_star);
  
  mat XZ_star = join_rows(X_star,z_star.t());
  mat Dxz_star = distMat(XZ_star);
  mat Rxz_star = matern(nu_y,1.,phi_y,Dxz_star);
  
  // extract Z(_tilde) via Z_star
  mat small_r(s_star,s);                                
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_z, 1., phi_z , sqrt(sum(square(X.row(j) - X_star.row(i) ))) ); 
  
  mat z = small_r.t() * inv(Rx_star) * z_star.t(); // expected value of z | z_star --- it's translated wrt z_star!
  
  mat XZ = join_rows(X,z);
  
  // compute the inverse of Exz via Exz_star only
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_y, 1., phi_y , sqrt(sum(square(XZ.row(j) - XZ_star.row(i) ))) ); 
  
  mat tmp = trans(small_r) * solve(Rxz_star,small_r);
  mat reg_Rxz_tilde = diagmat( 1. - diagvec(tmp)) ; 
  // mat Exz_tilde = (tmp + reg_Rxz_tilde) * sigma2_y;
  
  mat jnk = diagmat( 1./ (sigma2_y * diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part
  
  mat inv_Exz_tilde = inv( (Rxz_star/sigma2_y) + (small_r * jnk * trans(small_r) ) ) * small_r; // TODO why does it crashes with solve?
  inv_Exz_tilde = jnk * trans(small_r) * inv_Exz_tilde;
  inv_Exz_tilde.each_row() %= trans(diagvec(jnk)) ;
  inv_Exz_tilde = jnk - inv_Exz_tilde;
  
  // jnk - (jnk %*% t(small_r) %*% solve( (Rxz_star/sigma2_y) + small_r%*%jnk%*%t(small_r)) %*% small_r   %*% jnk       OR BETTER
  // jnk - (jnk %*% t(small_r) %*% solve( (Rxz_star/sigma2_y) + small_r%*%jnk%*%t(small_r)    , small_r)) * matrix(diag(jnk),s,s,byrow=TRUE)
  
  // Compute the determinant of Exz via Exz_star only
  double log_det_Exz_tilde,sign;
  log_det(log_det_Exz_tilde,sign, eye<mat>(s_star,s_star) + (small_r * jnk * (trans(small_r) * inv(Rxz_star/sigma2_y))) );
  log_det_Exz_tilde -= sum(log(jnk.diag()));
  
  // determinant( diag(s_star) + small_r %*% jnk %*% (t(small_r) %*% solve(Rxz_star/sigma2_y)) ,logarithm = TRUE)$modulus -sum(log(diag(jnk))) 
  
  // Compute the likelihood
  vec tmp_ll(n);
  for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( y.row(i) * inv_Exz_tilde  * trans(y.row(i)));
  
  double ll = -0.5*n*s*log(2*M_PI) -0.5*n*log_det_Exz_tilde -0.5*sum(tmp_ll);
  
  // std::cout << sum(log(jnk.diag())) << "  ---  " << log_det_Exz_tilde << "  ---  " << sum(sum(inv_Exz_tilde)) << std::endl;
  // std::cout << -0.5*n*s*log(2*M_PI) << "  ---  " << -0.5*n*log_det_Exz_tilde << "  ---  " << -0.5*sum(tmp_ll) << std::endl;
  
  return ll;
  
} // end log-likelihood

double predictive_int_t_lL(const mat &y, int n, const mat &X, const mat &X_star, const mat &z_star, double sigma2_y, double phi_y, double sigma2_z, double phi_z, const mat &Dx_star, double temp){ // tempered log likelihood ( z is passed as a/multiple row/s from a matrix)
  
  
  int s = y.n_cols;
  int s_star = z_star.n_cols;
  
  // compute the necessary matrices
  // mat Ex = matern(nu_z,sigma2_z,phi_z,Dx);
  mat Rx_star = matern(nu_z,1.,phi_z,Dx_star);
  
  mat XZ_star = join_rows(X_star,z_star.t());
  mat Dxz_star = distMat(XZ_star);
  mat Rxz_star = matern(nu_y,1.,phi_y,Dxz_star);
  
  // extract Z(_tilde) via Z_star
  mat small_r(s_star,s);                                
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_z, 1., phi_z , sqrt(sum(square(X.row(j) - X_star.row(i) ))) ); 
  
  mat z = small_r.t() * inv(Rx_star) * z_star.t(); // expected value of z | z_star
  
  mat XZ = join_rows(X,z);
  
  // compute the inverse of Exz via Exz_star only
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_y, 1., phi_y , sqrt(sum(square(XZ.row(j) - XZ_star.row(i) ))) ); 
  
  mat tmp = trans(small_r) * solve(Rxz_star,small_r);
  mat reg_Rxz_tilde = diagmat( 1. - diagvec(tmp)) ; 
  // mat Exz_tilde = (tmp + reg_Rxz_tilde) * sigma2_y;
  
  mat jnk = diagmat( 1./ (sigma2_y * diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part
  
  mat inv_Exz_tilde = inv( (Rxz_star/sigma2_y) + (small_r * jnk * trans(small_r) ) ) * small_r; // TODO why does it crashes with solve?
  inv_Exz_tilde = jnk * trans(small_r) * inv_Exz_tilde;
  inv_Exz_tilde.each_row() %= trans(diagvec(jnk)) ;
  inv_Exz_tilde = jnk - inv_Exz_tilde;
  
  // jnk - (jnk %*% t(small_r) %*% solve( (Rxz_star/sigma2_y) + small_r%*%jnk%*%t(small_r)) %*% small_r   %*% jnk       OR BETTER
  // jnk - (jnk %*% t(small_r) %*% solve( (Rxz_star/sigma2_y) + small_r%*%jnk%*%t(small_r)    , small_r)) * matrix(diag(jnk),s,s,byrow=TRUE)
  
  // Compute the determinant of Exz via Exz_star only
  double log_det_Exz_tilde,sign;
  log_det(log_det_Exz_tilde,sign, eye<mat>(s_star,s_star) + (small_r * jnk * (trans(small_r) * inv(Rxz_star/sigma2_y))) );  // TODO CHECK THIS EXPRESSION ( inv(Rxz_star / sigma2 !!! ))
  log_det_Exz_tilde -= sum(log(jnk.diag()));
  
  // determinant( diag(s_star) + small_r %*% jnk %*% (t(small_r) %*% solve(Rxz_star/sigma2_y)) ,logarithm = TRUE)$modulus -sum(log(diag(jnk))) 
  
  // Compute the likelihood
  vec tmp_ll(n);
  for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( y.row(i) * inv_Exz_tilde  * trans(y.row(i)));
  
  double ll = -0.5*n*s*log(2*M_PI) -0.5*n*log_det_Exz_tilde -0.5*sum(tmp_ll);
  
  // std::cout << sum(log(jnk.diag())) << "  ---  " << log_det_Exz_tilde << "  ---  " << sum(sum(inv_Exz_tilde)) << std::endl;
  // std::cout << -0.5*n*s*log(2*M_PI) << "  ---  " << -0.5*n*log_det_Exz_tilde << "  ---  " << -0.5*sum(tmp_ll) << std::endl;
  
  return ll/temp;
  
} //end tempered log-likelihood


/* #########################################################################
############################################################################
##########    Ell Slice Sampler for the predictive model    ################
############################################################################
############################################################################ */


mat elliptical_ss_predictive_int(mat Z, double &ll, const mat &y, const mat &X, const mat &X_star, double sigma2_y,double phi_y,double sigma2_z, double phi_z, const mat &Dx_star, double temp, double &count){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  int n = y.n_rows;
  
  mat Sigma = matern(nu_z,sigma2_z,phi_z,Dx_star); // note that now the covariance matrix is defined on the * space, cause our parameters are the z*
  
  //Choose the Ellipse(s)
  mat nu(p,s);
  mat z_next = Z;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found;

  mat chol_Sigma;
  bool check = chol(chol_Sigma,Sigma); // THIS IS STILL S^3, but we can potentially modify this without computing chol(matern(Dx)) for each change of parameters, I think
  if(!check){
    // ll = -std::numeric_limits<double>::infinity();
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
    
  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock
  
  for(int i=0; i<p; ++i){
    nu.row(i) =  normal_01(s).t() * chol_Sigma;
  }
  
  omp_unset_lock(&RNGlock); // ************************ lock
  
  
  // Then for each Z, run Ell SS
  
  for(int i=0; i<p; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above
    
    omp_set_lock(&RNGlock); // ************************ lock
    
    //ll threshold
    threshold = ll + log(random_01(rng));  
    
    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;
    delta = theta_max-theta_min;
    
    omp_unset_lock(&RNGlock); // ************************ lock
    
    found = false;
    
    while(!found){

      count++;
      
      z_next.row(i) = Z.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = predictive_int_t_lL(y, n, X, X_star, z_next, sigma2_y, phi_y, sigma2_z, phi_z, Dx_star, temp);  
      
      if( !std::isfinite(ll_next) ){
        //Z and ll unchanged
        found = true;
      } 
      
      if(!found){ // to avoid comparisons with NAs
        
        if( ll_next >= threshold ){
          
          Z.row(i) = z_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;
          
          omp_set_lock(&RNGlock); // ************************ lock
          theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
    
  }
  
  return Z;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}


/* ######################################################################## */
/* ######################################################################## */
/* ##### Model functions for predictive models w/o integrating y_star ##### */
/* ######################################################################## */
/* ######################################################################## */


/****************************************************************************/
/***************** lL without integrating y_star ****************************/
/****************************************************************************/

// we expect worse mixing but their comp complexity is linear in s and square in s_star

double predictive_notint_lL(const mat &y, int n, const mat &X, const mat &X_star, const mat &z_star, const mat &y_star_rng, double sigma2_y, double phi_y, double sigma2_z, double phi_z, const mat &Dx_star){ // log likelihood ( z is passed as a/multiple row/s from a matrix)

  int s = y.n_cols;
  int s_star = z_star.n_cols;
  
  // compute the necessary matrices
  // mat Ex = matern(nu_z,sigma2_z,phi_z,Dx);
  mat Rx_star = matern(nu_z,1.,phi_z,Dx_star);
  
  mat XZ_star = join_rows(X_star,z_star.t());
  mat Dxz_star = distMat(XZ_star);
  mat Rxz_star = matern(nu_y,1.,phi_y,Dxz_star);

  //recover the y_star
  mat y_star = y_star_rng * chol(sigma2_y*Rxz_star); // n x s* or 1xs*

  // extract Z(_tilde) via Z_star
  mat small_r(s_star,s);                                
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_z, 1., phi_z , sqrt(sum(square(X.row(j) - X_star.row(i) ))) ); 
  
  vec z = small_r.t() * inv(Rx_star) * z_star.t(); // expected value of z | z_star
  mat XZ = join_rows(X,z);

  // extract y(_tilde) via y_star
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( nu_y, 1., phi_y , sqrt(sum(square(XZ.row(j) - XZ_star.row(i) ))) ); 
  
  mat y_tilde = trans( small_r.t() * inv(Rxz_star) * y_star.t() ); // expected value of y | y_star (n x s)

  // now the idea is that my data are distributed according to Y = \tilde y + \varepsilon , where \varepsilon is a spacial-homogeneous error, in our case the correction_for_bias
  mat tmp = trans(small_r) * solve(Rxz_star,small_r);  //this is Rxz_tilde withut regularization
  mat reg_Rxz_tilde = diagmat( 1. - diagvec(tmp)) ;   // this is the regularization part    ----- Exz_tilde is ...
  // mat Exz_tilde = (tmp + reg_Rxz_tilde) * sigma2_y;

  // invert the bias_correction part and compute its determinant
  mat inv_reg_Exz_tilde = diagmat( 1./ (sigma2_y * diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part, variance sigma2_y taken into account
  double log_det_reg_Exz_tilde = -sum(log(inv_reg_Exz_tilde.diag()));           // this is its determinant (I used the inv_ so that sigma2_y si there already)

  // Compute the likelihood
  vec tmp_ll(n);
  
  // for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( (y.row(i) - y_tilde.row(i)) * inv_reg_Exz_tilde  * trans(y.row(i) - y_tilde.row(i)));  // this should be optimized on arma but to be sure..
  
  if( y_tilde.n_rows > 1 ) // here I assume we can be just in one of the cases [either one y_star per particle or one y_star per observation per particle ] 
    for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( ( y.row(i) - y_tilde.row(i) ) % trans( diagvec(inv_reg_Exz_tilde) ) ) * trans( y.row(i) - y_tilde.row(i) ) );
  else
    for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( ( y.row(i) - y_tilde.row(0) ) % trans( diagvec(inv_reg_Exz_tilde) ) ) * trans( y.row(i) - y_tilde.row(0) ) );

  double ll = -0.5*n*s*log(2*M_PI) -0.5*n*log_det_reg_Exz_tilde -0.5*sum(tmp_ll);
  
  return ll;

} //end log-likelihood with nonintegrated y_star


double predictive_notint_t_lL(const mat &y, int n, const mat &X, const mat &X_star, const mat &z_star, const mat &y_star, double sigma2_y, double phi_y, double sigma2_z, double phi_z, const mat &Dx_star, double temp){ // tempered log likelihood ( z is passed as a/multiple row/s from a matrix)

  int s = y.n_cols;
  int s_star = z_star.n_cols;
  
  // compute the necessary matrices
  // mat Ex_star = matern(ni_prior,sigma2_z,phi_z,Dx_star);
  mat Rx_star = matern(ni_prior,1.,phi_z,Dx_star);
  
  mat XZ_star = join_rows(X_star,z_star.t());
  mat Dxz_star = distMat(XZ_star);
  mat Rxz_star = matern(ni_likelihood,1.,phi_y,Dxz_star);  // Wasted computations!!! I do this outside as well!

  // y_star is passed as a parameter now
  // mat y_star = y_star_rng * chol(sigma2_y*Rxz_star); // n x s* or 1 x s*

  // extract Z(_tilde) via Z_star
  mat small_r(s_star,s);                                
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( ni_prior, 1., phi_z , sqrt(sum(square(X.row(j) - X_star.row(i) ))) ); 
  
  vec z_tilde = small_r.t() * inv(Rx_star) * z_star.t(); // expected value of z | z_star
  mat XZ = join_rows(X,z_tilde);

  // extract y(_tilde) via y_star
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( ni_likelihood, 1., phi_y , sqrt(sum(square(XZ.row(j) - XZ_star.row(i) ))) ); 

  mat y_tilde = trans( small_r.t() * inv(Rxz_star) * y_star.t() ); // expected value of y | y_star (n x s or 1 x s)

  // now the idea is that my data are distributed according to Y = \tilde y + \varepsilon , where \varepsilon is a spacial-homogeneous error, in our case the correction_for_bias
  mat tmp = trans(small_r) * solve(Rxz_star,small_r);  //this is Rxz_tilde withut regularization
  mat reg_Rxz_tilde = diagmat( 1. - diagvec(tmp)) ;   // this is the regularization part    ----- Exz_tilde is ...
  // mat Exz_tilde = (tmp + reg_Rxz_tilde) * sigma2_y;

  // invert the bias_correction part and compute its determinant
  mat inv_reg_Exz_tilde = diagmat( 1./ (sigma2_y * diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part, variance sigma2_y taken into account
  double log_det_reg_Exz_tilde = -sum(log(inv_reg_Exz_tilde.diag()));           // this is its determinant (I used the inv_ so that sigma2_y si there already)

  // Compute the likelihood
  vec tmp_ll(n);
  // for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( (y.row(i) - y_tilde.row(i)) * inv_reg_Exz_tilde  * trans(y.row(i) - y_tilde.row(i)));  // this should be optimized on arma but to be sure..
  
  if( y_tilde.n_rows > 1 ){ // here I assume we can be just in one of the cases [either one y_star per particle or one y_star per observation per particle ] 
    // for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( ( y.row(i) - y_tilde.row(i) ) % trans( diagvec(inv_reg_Exz_tilde) ) ) * trans( y.row(i) - y_tilde.row(i) ) ); // TODO CHECK IS THIS THE SAME AS SIMPLY MULLTIPLY THE MATRICES NORMALLY?
    for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( y.row(i) - y_tilde.row(i) ) * diagmat(inv_reg_Exz_tilde) * trans( y.row(i) - y_tilde.row(i) ) ); // Apparently arma is optimized for this
  }else{
    // for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( ( y.row(i) - y_tilde.row(0) ) % trans( diagvec(inv_reg_Exz_tilde) ) ) * trans( y.row(i) - y_tilde.row(0) ) );
    for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( y.row(i) - y_tilde.row(0) ) * diagmat(inv_reg_Exz_tilde) * trans( y.row(i) - y_tilde.row(0) ) ); // Apparently arma is optimized for this
  }

  // mat mu(n,s);
  // if( y_tilde.n_rows == 1 ){
  //   for(int i=0; i<n; ++i) mu.row(i) = y_tilde.row(0);
  // }else mu = y_tilde;
  // for(int i=0; i<n; ++i) tmp_ll(i) = as_scalar( ( ( y.row(i) - mu.row(i) ) % trans( diagvec(inv_reg_Exz_tilde) ) ) * trans( y.row(i) - mu.row(i) ) );


  double ll = -0.5*n*s*log(2*M_PI) -0.5*n*log_det_reg_Exz_tilde -0.5*sum(tmp_ll);
  
  return ll/temp;

} //end tempered log-likelihood with nonintegrated y_star


double lp_notint(const mat &z,int p,double sigma2_y,double phi_y,double sigma2_z,double phi_z, mat y_star, const mat &X_star, double shape_p_y,double rate_p_y,double shape_s_y,double rate_s_y,double shape_p_z,double rate_p_z,double shape_s_z,double rate_s_z, mat Dx){
  
  int s = z.n_cols;

  // Priors
  double log_p_s_z,log_p_phi_z,log_p_z,log_p_y_star;
  
  if(p > 0){ 
    // Sigma_z (this could be with Dan Simpsons proposal)
    if(sigma2_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_s_z = (shape_s_z-1)*log(sigma2_z) -rate_s_z*sigma2_z + shape_s_z*log(rate_s_z) - boost::math::lgamma(shape_s_z);
    
    //phi_z
    if(phi_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_phi_z = (shape_p_z-1)*log(phi_z) -rate_p_z*phi_z + shape_p_z*log(rate_p_z) - boost::math::lgamma(shape_p_z);
    
    
    // Z|sigma_z,phi_z
    mat Ex = matern(ni_prior,sigma2_z,phi_z,Dx);
    mat iEx; bool check = inv_sympd(iEx,Ex);
    if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0
    
    log_p_z = 0.;
    double log_det_Ex,sign;
    log_det(log_det_Ex,sign,Ex);
    
    for(int i=0; i<p; ++i){
      log_p_z += -0.5*s*log(2*M_PI) -0.5*log_det_Ex -0.5*as_scalar( (z.row(i)*iEx)*z.row(i).t() );
    }
    
  }else{
    log_p_s_z = log_p_phi_z = log_p_z = 0.; //otherwise set to a constant so that the it can continue without taking Z into account
  }
  
  // phi_y
  if(phi_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_phi_y = (shape_p_y-1)*log(phi_y) -rate_p_y*phi_y + shape_p_y*log(rate_p_y) - boost::math::lgamma(shape_p_y);
  
  // sigma 2 y
  if(sigma2_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_s_y = (shape_s_y-1)*log(sigma2_y) -rate_s_y*sigma2_y + shape_s_y*log(rate_s_y) - boost::math::lgamma(shape_s_y);
  
  // y_star rng
  mat Exz_star = matern(ni_likelihood,sigma2_y,phi_y,distMat(join_rows(X_star,z.t()))); // z is actually Z_star TODO maybe renae thm here?
  
  mat iExz; bool check = inv_sympd(iExz,Exz_star);
  if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0
    
  log_p_z = 0.;
  double log_det_Exz,sign2;
  log_det(log_det_Exz,sign2,Exz_star);
    
  log_p_y_star = -0.5*s*log(2*M_PI) -0.5*log_det_Exz -0.5*as_scalar( y_star.row(0) * iExz * trans(y_star.row(0)) );

  // Joint prior
  double lp = ( log_p_s_z + log_p_phi_z + log_p_z + log_p_phi_y + log_p_s_y + log_p_y_star);
  
  return lp;
} //log prior


/* #########################################################################
##    Ell Slice Sampler for the predictive model w/o integrating y_star  ###
############################################################################ */


mat elliptical_ss_predictive_notint(mat Z, double &ll, const mat &y, const mat &X, const mat &X_star, const mat &y_star_rng, double sigma2_y,double phi_y,double sigma2_z, double phi_z, const mat &Dx_star, double temp, double &count){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  int n = y.n_rows;
  int n_y = y_star_rng.n_rows;
  
  mat Sigma = matern(nu_z,sigma2_z,phi_z,Dx_star); // note that now the covariance matrix is defined on the * space, cause our parameters are the z*
  
  //Choose the Ellipse(s)
  mat nu(p,s);
  mat z_next = Z;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found;

  mat chol_Sigma;
  bool check = chol(chol_Sigma,Sigma); // THIS IS STILL S^3, but we can potentially modify this without computing chol(matern(Dx)) for each change of parameters, I think
  if(!check){
    // ll = -std::numeric_limits<double>::infinity();
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  
  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock
  
  for(int i=0; i<p; ++i){
    nu.row(i) = normal_01(s).t() * chol_Sigma;
  }
  
  omp_unset_lock(&RNGlock); // ************************ lock
  
  
  // Then for each Z, run Ell SS
  
  for(int i=0; i<p; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above
    
    omp_set_lock(&RNGlock); // ************************ lock
    
    //ll threshold
    threshold = ll + log(random_01(rng));  
    
    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;
    delta = theta_max-theta_min;
    
    omp_unset_lock(&RNGlock); // ************************ lock
    
    found = false;
    
    while(!found){

      count++;
      
      // -- proposed Z
      z_next.row(i) = Z.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = predictive_notint_t_lL(y, n, X, X_star, z_next, y_star_rng, sigma2_y, phi_y, sigma2_z, phi_z, Dx_star, temp);  
      
      if( !std::isfinite(ll_next) ){
        //Z and ll unchanged
        found = true;
      } 
      
      if(!found){ // to avoid comparisons with NAs
        
        if( ll_next >= threshold ){
          
          Z.row(i) = z_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;
          
          omp_set_lock(&RNGlock); // ************************ lock
          theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
    
  }
  
  return Z;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}

// deprecated!!!
mat y_star_ellSS(const mat &Z, double &ll, const mat &y, const mat &X, const mat &X_star, mat y_star_rng, double sigma2_y,double phi_y,double sigma2_z, double phi_z, const mat &Dx_star, double temp){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  // deprecated!!!
  int s_star = y_star_rng.n_cols;
  int n = y_star_rng.n_rows;// or int n = y.n_rows;
  
  //Choose the Ellipse(s)
  mat nu(n,s_star);
  mat y_star_rng_next = y_star_rng;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found;

  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock
  
  for(int i=0; i<n; ++i){
    nu.row(i) = normal_01(s_star).t();
  }
  
  omp_unset_lock(&RNGlock); // ************************ lock
  
  
  // Then for each y_star_rng, run Ell SS
  
  for(int i=0; i<n; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above
    
    omp_set_lock(&RNGlock); // ************************ lock
    
    //ll threshold
    threshold = ll + log(random_01(rng));  
    
    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;
    delta = theta_max-theta_min;
    
    omp_unset_lock(&RNGlock); // ************************ lock
    
    found = false;
    
    while(!found){
      
      // -- proposed Z
      y_star_rng_next.row(i) = y_star_rng.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = predictive_notint_t_lL(y, n, X, X_star, Z, y_star_rng_next, sigma2_y, phi_y, sigma2_z, phi_z, Dx_star, temp);  
      
      if( !std::isfinite(ll_next) ){
        //y_star_rng and ll unchanged
        found = true;
      } 
      
      if(!found){ // to avoid comparisons with NAs
        
        if( ll_next >= threshold ){
          
          y_star_rng.row(i) = y_star_rng_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;
          
          omp_set_lock(&RNGlock); // ************************ lock
          theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
    
  }
  
  return y_star_rng;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}

/* #########################################################################
########################  Gibbs step for y_star   ##########################
############################################################################ */

mat gibbs_step_ystar_ybar(const mat &y, int n, const mat &X, const mat &X_star, const mat &z_star, const mat &y_star, double sigma2_y, double phi_y, double sigma2_z, double phi_z, const mat &Dx_star){ // log likelihood ( z is passed as a/multiple row/s from a matrix)

  int s = y.n_cols;
  int s_star = z_star.n_cols;
  
  // compute the necessary matrices
  // mat Ex = matern(nu_z,sigma2_z,phi_z,Dx);
  mat Rx_star = matern(ni_prior,1.,phi_z,Dx_star);
  
  mat XZ_star = join_rows(X_star,z_star.t());
  mat Dxz_star = distMat(XZ_star);
  mat Rxz_star = matern(ni_likelihood,1.,phi_y,Dxz_star);

  mat ic_Exz_star = inv(chol(sigma2_y*Rxz_star));

  // extract Z(_tilde) via Z_star
  mat small_r(s_star,s);                                
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( ni_prior, 1., phi_z , sqrt(sum(square(X.row(j) - X_star.row(i) ))) ); 
  
  vec z_tilde = small_r.t() * inv(Rx_star) * z_star.t(); // expected value of z | z_star
  mat XZ = join_rows(X,z_tilde);

  // extract y(_tilde) via y_star
  for(int j=0; j<s; ++j) for(int i=0; i<s_star; ++i)  small_r(i,j) = matern_point( ni_likelihood, 1., phi_y , sqrt(sum(square(XZ.row(j) - XZ_star.row(i) ))) ); 
  
  // now the idea is that my data are distributed according to Y = \tilde y + \varepsilon , where \varepsilon is a spacial-homogeneous error, in our case the correction_for_bias
  mat tmp = trans(small_r) * solve(Rxz_star,small_r);  //this is Rxz_tilde withut regularization
  mat reg_Rxz_tilde = diagmat( 1. - diagvec(tmp)) ;   // this is the regularization part    ----- Exz_tilde is ...
  // mat Exz_tilde = (tmp + reg_Rxz_tilde) * sigma2_y;

  // invert the bias_correction part and compute its determinant
  // mat inv_reg_Rxz_tilde = diagmat( 1./ ( diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part, variance sigma2_y taken into account
  mat inv_reg_Exz_tilde = diagmat( 1./ (sigma2_y * diagvec( reg_Rxz_tilde )) ); // this is the inverse of the regularization part, variance sigma2_y taken into account

  mat inv_Rxz_star = inv( Rxz_star ); 
  mat inv_Exz_star = inv_Rxz_star / sigma2_y; //these are just s_star x s_star so it should be fast!


  // mean_y = trans( small_r.t() * inv(Rxz_star) * y_star.t() )    AND      var_y = diagmat(inv_reg_Exz_tilde)
  // mean_y* = 0     AND     var_y* = sigma2_y*Rxz_star
  // so ...

  // tmp = small_r;
  // tmp.each_row() %= trans(diagvec(inv_reg_Exz_tilde));                            // fast post multiplication as inv_reg_Exz_tilde is diagonal
  // mat V = inv( inv_Exz_star + ((double)n * inv_Rxz_star * (tmp * trans(small_r)) * inv_Rxz_star) ); 

  mat V = inv( inv_Exz_star + ((double)n * inv_Rxz_star * small_r * diagmat(inv_reg_Exz_tilde) * trans(small_r) * inv_Rxz_star) ); 
  mat c_V = chol(V);

  rowvec mu, tmp_vec;
  mat new_y_star(1,s_star);     

  // mu = (double)n * trans(V * inv_Rxz_star * tmp * trans( mean(y,0) )); // 1x1 s*xs* s*xs*  s*xs sx1 = s*x1  --> so trans() to rowvec
  mu = trans(V * ( /* inv_Exz_star * zeros<vec>(s_star) + */ (double)n * trans(small_r.t() * inv_Rxz_star) * diagmat(inv_reg_Exz_tilde) * trans( mean(y,0) ))); // --> so trans() to rowvec

    omp_set_lock(&RNGlock);  // ************************ lock
      tmp_vec = normal_01(s_star).t();
    omp_unset_lock(&RNGlock); // ************************ lock

    new_y_star.row(0) = (tmp_vec * c_V + mu); // 1xs* s*xs* + 1xs* = 1xs*

  return new_y_star; // s_star x 1

} //end gibbs proposal for nonintegrated y_star

/* #########################################################################
############################################################################
###############    Dim Exp  Main PREDICTIVE ITEGRATED   ####################
############################################################################
############################################################################ */

/* #########################################################################
########################  No Expansion, baseline main   ####################
############################################################################ */

Rcpp::List baseline_predictive_int_SMC(int n_part, mat y, mat X, mat X_star, int n, int p, int s_star, double factor_temp ,double thresh, int det_schedule, int M, int J,
                                   double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y)
{
  omp_init_lock(&RNGlock);  

  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part);
  mat theta(n_part,2); mat theta_new(n_part,2);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  cube post_Dxz_star(s_star,s_star,n_part);
  cube post_Exz_star(s_star,s_star,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  mat tmpMat(n_part,s_star);
  vec tmpVec(n_part);
  mat tmp_Ex(s_star,s_star);

  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(2,2);
  mat chol_Sigma_prop = eye<mat>(2,2);
  mat old_Sigma_prop = eye<mat>(2,2);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);
  
  // Dummy zeta vars
  mat dummy_z = zeros<mat>(1,s_star);
  double dummy_p_z = 1.; double dummy_s_z = 1.;
  double shape_p_z = 1.; double rate_p_z = 1.;double shape_s_z = 1.;double rate_s_z = 1.;
  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... ";
  
  mat Dx = distMat(X);
  mat Dx_star = distMat(X_star);
  
  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  
  mat Sigma_prop_0 = old_Sigma_prop;
  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = lp(dummy_z,0,exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
    ll_curr(j) = predictive_int_t_lL(y,n,X,X_star,dummy_z,exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,Dx_star,temp(i));
  }
  
  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem) temp.resize(2*temp.n_elem);
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp(lw,ll_tmp,temp(i-1),ess,ess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }

    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviour!";
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 

    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    /*
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 
    
    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** ESS ********
    ess = ESS_log(lw);
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0.);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = multi_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      lp_y = lp_y.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    // TODO now could be done better...
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(1,0) = Sigma_prop(0,1);
    
    Sigma_prop = factor_sd * Sigma_prop;
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
  
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
  
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;

    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      Sigma_prop = old_Sigma_prop;
      chol(chol_Sigma_prop,Sigma_prop);
    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*2);
      tmp_mat.resize(n_part,2);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      theta = join_horiz(lsigma2_y,lp_y);
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = predictive_int_t_lL(y,n,X,X_star,dummy_z,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,Dx_star,temp(i));
        lprior_new(j) = lp(dummy_z,0,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
        
      }  
      
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }  
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****   would it be faster with a for loop parallelized?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    
    //  ********* END full-MCMC MOVE (no move on the Zs now as there are none)
    
    
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_Dxz_star.slice(j) = Dx_star;
    post_Exz_star.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); //cant I just translate X at the start?
  }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }
  
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);

  mat post_mean_Exz_star = zeros<mat>(s_star,s_star);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz_star = post_mean_Exz_star + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); 
  }
  post_mean_Exz_star = post_mean_Exz_star/(double)n_part;

  Rcpp::List out = Rcpp::List::create(Rcpp::Named("post_mean_Exz_star")=post_mean_Exz_star,Rcpp::Named("post_Exz_star")=post_Exz_star,
  												  Rcpp::Named("p")=p,Rcpp::Named("X_star")=X_star,Rcpp::Named("Z_star")=dummy_z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=1.,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=1., Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp);
  return out;
  
}


/* #########################################################################
####################    Dim Exp  SMC fixed p     ###########################
############################################################################ */

Rcpp::List DE_predictive_int_SMC(int n_part, mat y, mat X, mat X_star, int n, int p, int s_star, double factor_temp ,double thresh, int det_schedule, int M, int J,
                             double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                             double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  
  // int s_star = X_star.n_rows;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper_Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  
  cube Z = zeros<cube>(p,s_star,n_part);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part); vec lsigma2_z(n_part);vec lp_z(n_part);
  mat theta(n_part,4); mat theta_new(n_part,4);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  cube post_Dxz_star(s_star,s_star,n_part);
  cube post_Exz_star(s_star,s_star,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  vec essCount(n);
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  mat tmp_Ex(s_star,s_star), c_Ex(s_star,s_star);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(4,4);;
  mat chol_Sigma_prop = eye<mat>(4,4);;
  mat old_Sigma_prop = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);
  
  vec tmp_eigval;
  mat tmp_eigvec;
  
  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... Z_star ... ";
  
  mat Dx = distMat(X);
  mat Dx_star = distMat(X_star);
  
  // sigma_z & phi_z
  lsigma2_z = log(random_gamma(n_part,sigma_z_prior)); //lsigma2_z = log(1.) + normal_01(n_part)/500.; //DEBUG
  lp_z = log(random_gamma(n_part,phi_z_prior)); //lp_z = log(0.5) + normal_01(n_part)/500.; //DEBUG
  
  // Z
  cube tmp_Z= zeros<cube>(p,s_star,n_part) ;
  
  #pragma omp parallel for private(tmp_Ex,c_Ex)
  for(int j=0; j<n_part; ++j){

      tmp_Ex = matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx_star);
      c_Ex = chol(tmp_Ex);

    for(int k=0; k<p; ++k){

      omp_set_lock(&RNGlock); // ************************ lock
        Z.slice(j).row(k) = normal_01(s_star).t(); // chol(tmp_Ex) * random std normals
      omp_unset_lock(&RNGlock); // ************************ lock

    }

    Z.slice(j) = Z.slice(j) * c_Ex;
  }
  
  std::cout << "Sigma and Phi ... ";
  
  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
    if(std::isnan(lsigma2_z(j)) || !std::isfinite(lsigma2_z(j))) lsigma2_z(j)=-DBL_MAX;
    if(std::isnan(lp_z(j)) || !std::isfinite(lp_z(j))) lp_z(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  old_Sigma_prop(2,2) = w_var(lsigma2_z,W);
  old_Sigma_prop(3,3) = w_var(lp_z,W);
  
  
  mat Sigma_prop_0 = old_Sigma_prop;
  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*(double)n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  std::cout << "Initial weights ... ";
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
    ll_curr(j) = predictive_int_t_lL(y,n,X,X_star,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),Dx_star,temp(i)); 
  }
  
  std::cout << "Done!" << std::endl; 
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem){ 
      temp.resize(2*temp.n_elem);
      essCount.resize(2*essCount.n_elem);
    }
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
    else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
    else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
    else{
      std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
      det_schedule = 1;
      temp(i) = std::max(factor_temp*temp(i-1),1.);
    }
    
    if( temp(i-1) == temp(i)){
      
      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }
      
    }else{
      count_stuck=0; // temp is moving, all good
    } 
    
    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
        std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    

    // ***** Weights ********

    
    /*  math
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 


    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i

    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << std::endl << "Ess= " << ess << std::endl << "Complete Degeneracy of the particles! Probably the cooling schedule was too fast...";
      
      temp.resize(i);
      return Rcpp::wrap(0.);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = resid_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      
      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    

    factor_sd = 2.38; //assuming gaussian, see Roberts 96, would be 2.38 something...actually a bit less for the optimal acc rate in dim 4 is less then 0.234
    // in our case though the variance is computed on a set that comes from a previous iter, even though is actually resampled to get to the current temp
    // so maybe a bit less would make sense...

   // weighted covariance matrix (too slow?).
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(2,2) = w_var(lsigma2_z,W);
    Sigma_prop(3,3) = w_var(lp_z,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(0,2) = w_cov(lsigma2_y,lsigma2_z,W);
    Sigma_prop(0,3) = w_cov(lsigma2_y,lp_z,W);
    
    Sigma_prop(1,2) = w_cov(lp_y,lsigma2_z,W);
    Sigma_prop(1,3) = w_cov(lp_y,lp_z,W);
    
    Sigma_prop(2,3) = w_cov(lsigma2_z,lp_z,W);
    
    Sigma_prop(1,0) = Sigma_prop(0,1);
    Sigma_prop(2,0) = Sigma_prop(0,2);
    Sigma_prop(3,0) = Sigma_prop(0,3);
    Sigma_prop(2,1) = Sigma_prop(1,2);
    Sigma_prop(3,1) = Sigma_prop(1,3);
    Sigma_prop(3,2) = Sigma_prop(2,3);

    // unweighted, but if we just resampled...then find nearest valid!
    theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
    // Sigma_prop = cov(theta);
    
    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;
    
    
    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      std::cout << " !! ";                        // find nearest covariance matrix to the one fond there TODO
      
      //TODO better, this is just a patch
      eig_sym(tmp_eigval,tmp_eigvec,Sigma_prop,"std");
      for(int ii=0; ii<4; ++ii){
        if( tmp_eigval(ii) <= 0) tmp_eigval(ii) = 1e-5;
      }
      Sigma_prop = tmp_eigvec * diagmat(tmp_eigval) * trans(tmp_eigvec);      

      if( !chol(chol_Sigma_prop,Sigma_prop) ){  // ... never lucky
        std::cout << " ?? ";                       

        Sigma_prop = old_Sigma_prop;

        chol(chol_Sigma_prop,Sigma_prop);

      }

    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*4);
      tmp_mat.resize(n_part,4);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      // theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);  // done already up
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = predictive_int_t_lL(y,n,X,X_star,Z.slice(j),exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),Dx_star,temp(i)); 
        lprior_new(j) = lp(Z.slice(j),p,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
      }
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)) + (theta_new(j,2) - theta(j,2)) + (theta_new(j,3) - theta(j,3)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      lsigma2_z = theta.col(2);
      lp_z = theta.col(3);
      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);
      
      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    
    if(p>0){
      //******************************************
      //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
      //******************************************

      essCount(i) = 0;

      
      for(int jj=0;jj<J;jj++){ // for more then one step...
        
        std::cout << "Ell_SS step #" <<jj+1 << " ... ";  
        
        #pragma omp parallel for
        for(int j=0; j<n_part; ++j){ //every particle
          
          Z.slice(j) = elliptical_ss_predictive_int( Z.slice(j), ll_curr(j), y, X, X_star, exp(lsigma2_y(j)), exp(lp_y(j)), exp(lsigma2_z(j)), exp(lp_z(j)), Dx_star, temp(i), essCount(i));
          
          // Note that ll_curr is updated by passing it by reference (I could actually do the same for Z)
          // lprior needs to be updated though 
          lprior_curr(j) = lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
          
        } // loop for every particle
      } // loop J times EllSliceSsamp
      
      essCount(i) = ((essCount(i) / n_part) / J )/ p ;

      std::cout << "(avg#steps " << essCount(i) << ") -- Done!" << endl;
      
      // Note there is no need to zero out the rest as for now the dimension is fixed
      
    } // if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same
    
    
    //  ********* END full-MCMC MOVE
    
    
    
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_Dxz_star.slice(j) = distMat(join_rows(X_star,trans(Z.slice(j))));
    post_Exz_star.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); //cant I just translate X at the start?
  }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
  
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);
  essCount.resize(i);

  mat post_mean_Exz_star = zeros<mat>(s_star,s_star);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz_star = post_mean_Exz_star + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); 
  }
  post_mean_Exz_star = post_mean_Exz_star/(double)n_part;

  Rcpp::List out = Rcpp::List::create(Rcpp::Named("post_mean_Exz_star")=post_mean_Exz_star,Rcpp::Named("post_Exz_star")=post_Exz_star,
  												  Rcpp::Named("p")=p,Rcpp::Named("X_star")=X_star,Rcpp::Named("Z_star")=Z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=lp_z, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp, Rcpp::Named("avg_ess_count")=mean(essCount));
  return out;
  
}



/* #########################################################################
############################################################################
#############    Dim Exp  Main PREDICTIVE NOT ITEGRATED   ##################
############################################################################
############################################################################ */

/* #########################################################################
########################  No Expansion, baseline main   ####################
############################################################################ */

Rcpp::List baseline_predictive_notint_SMC(int n_part, mat y, mat X, mat X_star, int n, int p, int s_star, int n_star, double factor_temp ,double thresh, int det_schedule, int M, int J,
                                   double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;

  // int s_star = X_star.n_rows;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  // Y_star, conditioned on the knots
  mat y_star = zeros<mat>(n_part,s_star);
  mat tmp_y_star = zeros<mat>(n_part,s_star);

  vec lsigma2_y(n_part); vec lp_y(n_part);
  mat theta(n_part,2); mat theta_new(n_part,2);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  cube post_Dxz_star(s_star,s_star,n_part);
  cube post_Exz_star(s_star,s_star,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  mat tmp_Ex(s_star,s_star); mat c_Ex(s_star,s_star);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(2,2);;
  mat chol_Sigma_prop = eye<mat>(2,2);;
  mat old_Sigma_prop = eye<mat>(2,2);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);

  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... Z_star ... ";
  
  mat Dx = distMat(X);
  mat Dx_star = distMat(X_star);


  // Dummy zeta vars
  mat dummy_z = zeros<mat>(1,s_star);
  double dummy_p_z = 1.; double dummy_s_z = 1.;
  double shape_p_z = 1.; double rate_p_z = 1.;double shape_s_z = 1.;double rate_s_z = 1.;


  std::cout << "Sigma and Phi ... ";
  
  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  
  mat Sigma_prop_0 = old_Sigma_prop;


  // *** Y_star
  mat tmp_Exz,c_Exz;

  #pragma omp parallel for private(tmp_Exz,c_Exz)
  for(int j=0; j<n_part; ++j){

    tmp_Exz = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X_star, dummy_z )));
    c_Exz = chol(tmp_Exz);

    omp_set_lock(&RNGlock); // ************************ lock
      y_star.row(j) = normal_01(s_star).t() * c_Exz;
    omp_unset_lock(&RNGlock); // ************************ lock

  }

  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  std::cout << "Initial weights ... ";
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){

    lprior_curr(j) = lp_notint(dummy_z,p,exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,y_star.row(j),X_star,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);

    ll_curr(j) = predictive_notint_t_lL(y,n,X,X_star,dummy_z,y_star.row(j),exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,Dx_star,temp(i)); 
  }
  
  std::cout << "Done!" << std::endl; 
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem) temp.resize(2*temp.n_elem);
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
    else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
    else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
    else{
      std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
      det_schedule = 1;
      temp(i) = std::max(factor_temp*temp(i-1),1.);
    }
    
    if( temp(i-1) == temp(i)){
      
      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }
      
    }else{
      count_stuck=0; // temp is moving, all good
    } 
    
    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
        std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    // ***** Weights ********
    
    /*  math
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << std::endl << "Ess= " << ess << std::endl << "Complete Degeneracy of the particles! Probably the cooling schedule was too fast...";
      
      temp.resize(i);
      return Rcpp::wrap(lw);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = multi_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_y_star = y_star;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        y_star.row(j) = tmp_y_star.row(idx(j)); 
      }

      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    // TODO now could be done better...
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(1,0) = Sigma_prop(0,1);
    
    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;
    
    
    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      Sigma_prop = old_Sigma_prop;
      chol(chol_Sigma_prop,Sigma_prop);
    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*2);
      tmp_mat.resize(n_part,2);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      theta = join_horiz(lsigma2_y,lp_y);
      theta_new = theta + tmp_mat;


      //******************************************
      //**** Y_STAR move !  **** 
      //******************************************
    
      // Gibbs step from the Full-Conditional
      tmp_y_star = y_star;
      #pragma omp parallel for
      for(int j=0; j<n_part; ++j){ //every particle

        tmp_y_star.row(j) = gibbs_step_ystar_ybar(y,n,X,X_star, dummy_z ,y_star.row(j) ,exp(lsigma2_y(j)), exp(lp_y(j)), dummy_s_z,dummy_p_z ,Dx_star); 

      } // loop for every particle



      //**** Joint Acceptance rate **** (joint for both parameters and y_star now)
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){

        lprior_new(j) = lp_notint(dummy_z,p,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,tmp_y_star.row(j),X_star,
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
        
        ll_new(j) = predictive_notint_t_lL(y,n,X,X_star,dummy_z,tmp_y_star.row(j) ,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,Dx_star,temp(i)); 
        
      }

      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      y_star.rows(idx) = tmp_y_star.rows(idx);

      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);      

      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);

      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    //  ********* END full-MCMC MOVE
    
    // EXTRA Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_Dxz_star.slice(j) = distMat(join_rows(X_star,dummy_z));
    post_Exz_star.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); //cant I just translate X at the start?
  }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
  
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  

  mat y_star_out = y_star;

  temp.resize(i);
  mat post_mean_Exz_star = zeros<mat>(s_star,s_star);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz_star = post_mean_Exz_star + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); 
  }
  post_mean_Exz_star = post_mean_Exz_star/(double)n_part;

  Rcpp::List out = Rcpp::List::create(Rcpp::Named("post_mean_Exz_star")=post_mean_Exz_star,Rcpp::Named("post_Exz_star")=post_Exz_star,
                            Rcpp::Named("p")=0,Rcpp::Named("X_star")=X_star,Rcpp::Named("Z_star")=dummy_z,Rcpp::Named("Y_star")=y_star_out,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=1.,Rcpp::Named("log_phi_z")=1.,
                                                  Rcpp::Named("ll_final")=ll_curr, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp);
  return out;
  
}


/* #########################################################################
####################    Dim Exp  SMC fixed p     ###########################
############################################################################ */

Rcpp::List DE_predictive_notint_ybar_SMC(int n_part, mat y, mat X, mat X_star, int n, int p, int s_star, double factor_temp ,double thresh, int det_schedule, int M, int J,
                             double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                             double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;

  // int s_star = X_star.n_rows;
  
  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper_Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  
  cube Z = zeros<cube>(p,s_star,n_part);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)

  // Y_star, conditioned on the knots
  mat y_star = zeros<mat>(n_part,s_star);
  mat tmp_y_star = zeros<mat>(n_part,s_star);

  vec lsigma2_y(n_part); vec lp_y(n_part); vec lsigma2_z(n_part);vec lp_z(n_part);
  mat theta(n_part,4); mat theta_new(n_part,4);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  cube post_Dxz_star(s_star,s_star,n_part);
  cube post_Exz_star(s_star,s_star,n_part);
  
  
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  double sum_lw;
  
  mat tmp_Ex(s_star,s_star); mat c_Ex(s_star,s_star);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(4,4);;
  mat chol_Sigma_prop = eye<mat>(4,4);;
  mat old_Sigma_prop = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);

  vec essCount(n);

  
  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // Sim from the prior
  std::cout << "Sim from the prior ... Z_star ... ";
  
  mat Dx = distMat(X);
  mat Dx_star = distMat(X_star);


  // sigma_z & phi_z
  lsigma2_z = log(random_gamma(n_part,sigma_z_prior)); //lsigma2_z = log(1.) + normal_01(n_part)/500.; //DEBUG
  lp_z = log(random_gamma(n_part,phi_z_prior)); //lp_z = log(0.5) + normal_01(n_part)/500.; //DEBUG
  
  // Z
  cube tmp_Z= zeros<cube>(p,s_star,n_part) ;
  
  #pragma omp parallel for private(tmp_Ex,c_Ex)
  for(int j=0; j<n_part; ++j){

      tmp_Ex = matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx_star);
      c_Ex = chol(tmp_Ex);

    for(int k=0; k<p; ++k){

      omp_set_lock(&RNGlock); // ************************ lock
        Z.slice(j).row(k) = normal_01(s_star).t() * c_Ex; // chol(tmp_Ex) * random std normals
      omp_unset_lock(&RNGlock); // ************************ lock

    }
  }
  
  std::cout << "Sigma and Phi ... ";
  
  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
    if(std::isnan(lsigma2_z(j)) || !std::isfinite(lsigma2_z(j))) lsigma2_z(j)=-DBL_MAX;
    if(std::isnan(lp_z(j)) || !std::isfinite(lp_z(j))) lp_z(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  old_Sigma_prop(2,2) = w_var(lsigma2_z,W);
  old_Sigma_prop(3,3) = w_var(lp_z,W);
  
  mat Sigma_prop_0 = old_Sigma_prop;


  // *** Y_star
  mat tmp_Exz,c_Exz;

  #pragma omp parallel for private(tmp_Exz,c_Exz)
  for(int j=0; j<n_part; ++j){

    tmp_Exz = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X_star, trans(Z.slice(j)) )));
    c_Exz = chol(tmp_Exz);

    omp_set_lock(&RNGlock); // ************************ lock
      y_star.row(j) = normal_01(s_star).t() * c_Exz;
    omp_unset_lock(&RNGlock); // ************************ lock

  }

  
  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  std::cout << "Initial weights ... ";
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){

    lprior_curr(j) = lp_notint(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),y_star.row(j),X_star,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);

    ll_curr(j) = predictive_notint_t_lL(y,n,X,X_star,Z.slice(j),y_star.row(j),exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),Dx_star,temp(i)); 
  }
  
  std::cout << "Done!" << std::endl; 
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    i++;
    if(i >= temp.n_elem){ 
      temp.resize(2*temp.n_elem);
      essCount.resize(2*essCount.n_elem);
    }
    
    cout << std::endl << "######## Iteration " << i << " " << endl;
    
    ll_tmp = ll_curr;
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
    else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
    else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, quite bad I'd say!)
    else{
      std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
      det_schedule = 1;
      temp(i) = std::max(factor_temp*temp(i-1),1.);
    }
    
    if( temp(i-1) == temp(i)){
      
      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }
      
    }else{
      count_stuck=0; // temp is moving, all good
    } 
    
    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
        std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    // ***** Weights ********
    
    /*  math
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << std::endl << "Ess= " << ess << std::endl << "Complete Degeneracy of the particles! Probably the cooling schedule was too fast...";
      
      temp.resize(i);
      return Rcpp::wrap(lw);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh || temp(i) ==1){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = multi_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      

      tmp_y_star = y_star;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        y_star.row(j) = tmp_y_star.row(idx(j)); 
      }

      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    // TODO now could be done better...
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(2,2) = w_var(lsigma2_z,W);
    Sigma_prop(3,3) = w_var(lp_z,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(0,2) = w_cov(lsigma2_y,lsigma2_z,W);
    Sigma_prop(0,3) = w_cov(lsigma2_y,lp_z,W);
    
    Sigma_prop(1,2) = w_cov(lp_y,lsigma2_z,W);
    Sigma_prop(1,3) = w_cov(lp_y,lp_z,W);
    
    Sigma_prop(2,3) = w_cov(lsigma2_z,lp_z,W);
    
    Sigma_prop(1,0) = Sigma_prop(0,1);
    Sigma_prop(2,0) = Sigma_prop(0,2);
    Sigma_prop(3,0) = Sigma_prop(0,3);
    Sigma_prop(2,1) = Sigma_prop(1,2);
    Sigma_prop(3,1) = Sigma_prop(1,3);
    Sigma_prop(3,2) = Sigma_prop(2,3);
    
    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck;
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;
    
    
    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      Sigma_prop = old_Sigma_prop;
      chol(chol_Sigma_prop,Sigma_prop);
    }
    
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*4);
      tmp_mat.resize(n_part,4);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
      theta_new = theta + tmp_mat;


      //******************************************
      //**** Y_STAR move !  **** 
      //******************************************
    
      // Gibbs step from the Full-Conditional
      tmp_y_star = y_star;
      #pragma omp parallel for
      for(int j=0; j<n_part; ++j){ //every particle

        tmp_y_star.row(j) = gibbs_step_ystar_ybar(y,n,X,X_star,Z.slice(j),y_star.row(j) ,exp(lsigma2_y(j)), exp(lp_y(j)), exp(lsigma2_z(j)), exp(lp_z(j)),Dx_star); 

      } // loop for every particle



      //**** Joint Acceptance rate **** (joint for both parameters and y_star now)
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){

        lprior_new(j) = lp_notint(Z.slice(j),p,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),tmp_y_star.row(j),X_star,
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
        
        ll_new(j) = predictive_notint_t_lL(y,n,X,X_star,Z.slice(j),tmp_y_star.row(j) ,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),Dx_star,temp(i)); 
        
      }

      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)) + (theta_new(j,2) - theta(j,2)) + (theta_new(j,3) - theta(j,3)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);
      y_star.rows(idx) = tmp_y_star.rows(idx);

      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);      

      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      lsigma2_z = theta.col(2);
      lp_z = theta.col(3);

      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  
      
      //weights stay the same
      
    } //end multiple MCMC steps
    
    
    if(p>0){
      //******************************************
      //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
      //******************************************

      essCount(i) = 0;
      
      for(int jj=0;jj<J;jj++){ // for more then one step...
        
        std::cout << "Ell_SS step #" <<jj+1 << " ... ";  
        
        #pragma omp parallel for
        for(int j=0; j<n_part; ++j){ //every particle

          Z.slice(j) = elliptical_ss_predictive_notint( Z.slice(j), ll_curr(j), y, X, X_star, y_star.row(j), exp(lsigma2_y(j)), exp(lp_y(j)), exp(lsigma2_z(j)), exp(lp_z(j)), Dx_star, temp(i), essCount(i));
          
          // Note that ll_curr is updated by passing it by reference (I could actually do the same for Z)
          // lprior needs to be updated though 
          lprior_curr(j) = lp_notint(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),y_star.row(j),X_star,
            shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx_star);
          
        } // loop for every particle
      } // loop J times EllSliceSsamp
      
      essCount(i) = ((essCount(i) / n_part) / J )/ p ;

      std::cout << "(avg#steps " << essCount(i) << ") -- Done!" << endl;
      
      // Note there is no need to zero out the rest as for now the dimension is fixed
      
    } // if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same

    //  ********* END full-MCMC MOVE
    
    // EXTRA Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_Dxz_star.slice(j) = distMat(join_rows(X_star,trans(Z.slice(j))));
    post_Exz_star.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); //cant I just translate X at the start?
  }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
  
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  

  mat y_star_out = y_star;

  temp.resize(i);
  essCount.resize(i);

  mat post_mean_Exz_star = zeros<mat>(s_star,s_star);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz_star = post_mean_Exz_star + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz_star.slice(j)); 
  }
  post_mean_Exz_star = post_mean_Exz_star/(double)n_part;

  Rcpp::List out = Rcpp::List::create(Rcpp::Named("post_mean_Exz_star")=post_mean_Exz_star,Rcpp::Named("post_Exz_star")=post_Exz_star,
  												  Rcpp::Named("p")=p,Rcpp::Named("X_star")=X_star,Rcpp::Named("Z_star")=Z,Rcpp::Named("Y_star")=y_star_out,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=lp_z, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp, Rcpp::Named("avg_ess_cout") = mean(essCount));
  return out;
  
}

/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 
/* ################################################################################################################################################################################################# */ 


/* ######################################################################## */
/* ######################################################################## */
/* ##########        Model functions for NNGP models      ################# */
/* ######################################################################## */
/* ######################################################################## */

// *********************** Likelihood and Prior functions

//this is an old version where the ordering is based just on the order in the data matrix, from 0 to s-1
double old_conditional_t_NN_lL(mat y ,mat X,mat z,double sigma2_y,double phi_y, double temp){ //tempered log likelihood (http://jonathantemplin.com/files/multivariate/mv11icpsr/mv11icpsr_lecture04.pdf p.34)
  
  int n = y.n_rows;
  int s = X.n_rows;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter
  // note that if n < m the model reduces to the classic GP modelling with n^3 cost rather than nm^2 cost
  

  // compute the distance matrix given Z
  mat Dxz = distMat(join_rows(X,z.t()));
  // mat Exz = matern(ni_likelihood,sigma2_y,phi_y,Dxz); //this would be the complete covariance matrix, but it's too high-dimensional
  
  // So what we do is looping through data-points from the reference set 
  // searching for nearest neighbors and defining SMALL matrices associated with them!

  // NOTE:
  // the reference set is now set to be the observed dataset, but could be a grid for example... it reduces to sublinear in n the following loop
  // making the procedure even cheaper!

 uvec Ns; uvec j_uvec(1);
 vec Csi_Nsi;
 rowvec Bsi;
 mat CNsi,ICNsi;
 double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf

 vec tmp_col;

 double ll = 0; // neutral value for the log likelihood to initialize it.

 for(int i=0; i<s; ++i){   // for each location

  // base case when i = 0, N(0) is th empty set
  if(i==0){ 
    condMean = 0;
    Fsi = sigma2_y;
    // empty set, the first point comes from an indep normal

    // increment the likelihood
    ll += -n*0.5*log(2*M_PI) -n*0.5*log(Fsi) -0.5 / Fsi * as_scalar( sum( square( y.col(i) - condMean ) ));

  }else{
    int n_Ns = std::min(i,m); // number of points in the neighbours set
    // catch the n_Ns neighbours
    Ns = sort_index(Dxz.col(i).subvec(0,i)); Ns = Ns.subvec(1,n_Ns);  // NB: index i will alway have 0 distance with himself! so first element should be discarded

    // if( i == floor(s/2) ){
    //   if( sum( Dxz(Ns(0),i) > Dxz.col(i) ) > 1 ) std::cout << ".";  // TODO REMOVE DEBUG 
    // }

    // compute quantities
    Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
    for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_likelihood, sigma2_y,phi_y,Dxz(Ns(j),i));

    CNsi = matern(ni_likelihood,sigma2_y,phi_y,Dxz.submat(Ns,Ns));
    bool check = inv_sympd(ICNsi,CNsi);
      if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

    Bsi = ( trans(Csi_Nsi) * ICNsi );
    Fsi = sigma2_y - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );
    
    // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
        if( Fsi <= 0 ) return -DBL_MAX;  //if it fails because of phi_y -> Inf of Simga_y -> 0

        // increment the likelihood
        for(int j=0; j<n; ++j){
          j_uvec(0) = j;
          ll += -0.5*log(2*M_PI) -0.5*log(Fsi) -0.5 / Fsi * as_scalar( square( y(j,i) - (Bsi * trans(y.submat(j_uvec,Ns)) ))) ;
        }  
    }

 }
    
  return ll/temp;
  
} //END tempered log-likelihood

double conditional_t_NN_lL(mat y ,mat X,mat z,double sigma2_y,double phi_y, double temp){ //tempered log likelihood (http://jonathantemplin.com/files/multivariate/mv11icpsr/mv11icpsr_lecture04.pdf p.34)
  
  int n = y.n_rows;
  int s = X.n_rows;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter
  // note that if s < m the model reduces to the classic GP modelling with s^3 cost rather than nm^2 cost
  

  int n_centroids = 1;
  uvec approx_order = linspace<uvec>(0,s-1,s);
  uvec chosen_idx(s);
  uvec free_idx(s);
  vec mean_dist(s);
  uvec tmp_vec_idx,tmp_single_idx;
  int tmp_idx;

  // compute the distance matrix given Z
  mat Dxz = distMat(join_rows(X,z.t()));
  // mat Exz = matern(ni_likelihood,sigma2_y,phi_y,Dxz); //this would be the complete covariance matrix, but it's too high-dimensional
  
  // So what we do is looping through data-points from the reference set 
  // searching for nearest neighbors and defining SMALL matrices associated with them!

  // NOTE:
  // the reference set is now set to be the observed dataset, but could be a grid for example... it reduces to sublinear in n the following loop
  // making the procedure even cheaper!

  // choose the starting point(s)
  mean_dist = mean(Dxz).t();

  tmp_vec_idx = stable_sort_index( mean_dist, "ascend" ); // if "ascend" gets the one with least distance, with "descend" get the most distants 

  chosen_idx.fill(s); chosen_idx.subvec(0,n_centroids-1) = approx_order.subvec(0,n_centroids-1);
  free_idx = linspace<uvec>(0,s-1,s);
  for(int j=0; j<n_centroids; ++j){
    tmp_single_idx = find(free_idx == chosen_idx(j),1);
    tmp_idx = tmp_single_idx(0);
    free_idx.shed_row( tmp_idx );
  } 

  // then get the others
  for(int j=n_centroids; j<s; ++j){
    
    //get distances of all the remaining points wrt the already selected ones
    mean_dist = mean(Dxz.submat( chosen_idx(span(0,j-1)) , free_idx )).t();

    tmp_single_idx = stable_sort_index(mean_dist, "ascend");
    tmp_idx = tmp_single_idx(0);
    approx_order(j) = free_idx(tmp_idx);
    chosen_idx(j) = approx_order(j);
    
    free_idx.shed_row( tmp_idx );
  }

  uvec Ns; uvec j_uvec(1);
  vec Csi_Nsi;
  rowvec Bsi;
  mat CNsi,ICNsi;
  double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf

  vec tmp_col;

  double ll = 0; // neutral value for the log likelihood to initialize it.

  for(int i=0; i<s; ++i){   // for each location

    // base case when i = 0, N(0) is th empty set
    if(i==0){ 

      condMean = 0;
      Fsi = sigma2_y;
      // empty set, the first point comes from an indep normal

      // increment the likelihood
      ll += -n*0.5*log(2*M_PI) -n*0.5*log(Fsi) -0.5 / Fsi * as_scalar( sum( square( y.col(approx_order(i)) - condMean ) ));

    }else{

      int n_Ns = std::min(i,m); // number of points in the neighbours set
      // catch the n_Ns neighbours
      Ns = stable_sort_index( Dxz.submat( approx_order.subvec(i,i) , approx_order.subvec(0,n_Ns-1) ) , "ascend");
      tmp_vec_idx = approx_order.subvec(0,n_Ns-1);
      Ns = tmp_vec_idx.elem(Ns);

      // compute quantities
      Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
      for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_likelihood, sigma2_y,phi_y,Dxz(Ns(j),approx_order(i)));

      CNsi = matern(ni_likelihood,sigma2_y,phi_y,Dxz.submat(Ns,Ns));
      bool check = inv_sympd(ICNsi,CNsi);
      if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

      Bsi = ( trans(Csi_Nsi) * ICNsi );
      Fsi = sigma2_y - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );
      
      // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
      if( Fsi <= 0 ) return -DBL_MAX;  //if it fails because of phi_y -> Inf of Simga_y -> 0

      // increment the likelihood
      for(int j=0; j<n; ++j){
        j_uvec(0) = j;
        ll += -0.5*log(2*M_PI) -0.5*log(Fsi) -0.5 / Fsi * as_scalar( square( y(j,approx_order(i)) - (Bsi * trans(y.submat(j_uvec,Ns)) ))) ;
      }  

    }

  } //end for each location
    
  return ll/temp;
  
} //END tempered log-likelihood

double t_NN_lL(mat y ,mat X,mat z,double sigma2_y,double phi_y, double temp, uvec approx_order, imat Nsets){ //tempered log likelihood (http://jonathantemplin.com/files/multivariate/mv11icpsr/mv11icpsr_lecture04.pdf p.34)
  
  int n = y.n_rows;
  int s = X.n_rows;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter
  // note that if n < m the model reduces to the classic GP modelling with n^3 cost rather than nm^2 cost
  

  // compute the distance matrix given Z
  mat Dxz = distMat(join_rows(X,z.t()));
  // mat Exz = matern(ni_likelihood,sigma2_y,phi_y,Dxz); //this would be the complete covariance matrix, but it's too high-dimensional
  
  // So what we do is looping through data-points from the reference set 
  // searching for nearest neighbors and defining SMALL matrices associated with them!

  // NOTE:
  // the reference set is now set to be the observed dataset, but could be a grid for example... it reduces to sublinear in n the following loop
  // making the procedure even cheaper!

 uvec Ns; uvec j_uvec(1);
 vec Csi_Nsi;
 rowvec Bsi;
 mat CNsi,ICNsi;
 double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf

 vec tmp_col;

 double ll = 0; // neutral value for the log likelihood to initialize it.

 for(int i=0; i<s; ++i){   // for each location

  // base case when i = 0, N(0) is th empty set
  if(i==0){ 
    condMean = 0;
    Fsi = sigma2_y;
    // empty set, the first point comes from an indep normal

    // increment the likelihood
    ll += -n*0.5*log(2*M_PI) -n*0.5*log(Fsi) -0.5 / Fsi * as_scalar( sum( square( y.col(approx_order(i)) - condMean ) ));

  }else{
    int n_Ns = std::min(i,m); // number of points in the neighbours set
    // catch the n_Ns neighbours
    Ns = conv_to<uvec>::from(Nsets.submat( i, 0, i, n_Ns-1 ));

    // compute quantities
    Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
    for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_likelihood, sigma2_y,phi_y,Dxz(Ns(j),approx_order(i)));

    CNsi = matern(ni_likelihood,sigma2_y,phi_y,Dxz.submat(Ns,Ns));
    bool check = inv_sympd(ICNsi,CNsi);
      if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

    Bsi = ( trans(Csi_Nsi) * ICNsi );
    Fsi = sigma2_y - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );
    
    // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
    if( Fsi <= 0 ) return -DBL_MAX;  //if it fails because of phi_y -> Inf of Simga_y -> 0

    // increment the likelihood
    for(int j=0; j<n; ++j){
      j_uvec(0) = j;
      ll += -0.5*log(2*M_PI) -0.5*log(Fsi) -0.5 / Fsi * as_scalar( square( y(j,approx_order(i)) - (Bsi * trans(y.submat(j_uvec,Ns)) ))) ;
    }  
  }

 } //end for each location
    
  return ll/temp;
  
} //END tempered log-likelihood

double NN_lp(mat z,int p,double sigma2_y,double phi_y,double sigma2_z,double phi_z,double shape_p_y,double rate_p_y,double shape_s_y,double rate_s_y,double shape_p_z,double rate_p_z,double shape_s_z,double rate_s_z, mat Dx, uvec approx_order_prior, imat Nsets_prior){
  
  int s = z.n_cols;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter
  // Priors
  double log_p_s_z,log_p_phi_z,log_p_z = 0.;
  
  if(p > 0)
  { 
    // Sigma_z (this could be with Dan Simpsons proposal)
    if(sigma2_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_s_z = (shape_s_z-1)*log(sigma2_z) -rate_s_z*sigma2_z + shape_s_z*log(rate_s_z) - boost::math::lgamma(shape_s_z);
    
    //phi_z
    if(phi_z<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
    log_p_phi_z = (shape_p_z-1)*log(phi_z) -rate_p_z*phi_z + shape_p_z*log(rate_p_z) - boost::math::lgamma(shape_p_z);
    
    
    // Z|sigma_z,phi_z
    // now the NNGP kicks in for Z as well. Notice that , as we are dealing with EllSliceSamp, the prior is computed WAY less than the lL, so we could avoid this step..
    // mat Ex = matern(ni_prior,sigma2_z,phi_z,Dx);
    // NOTE that here it might actually be worth to initialize the matrix and fill it as we compute the values, as with p>1 I will otherwise end up redoing some computations.
    // I DO NOT CARE FOR NOW

    //for(int k=0; k<p; ++k){  //TODO do we need this? as each extra dim is independent can we consider them as p draws from the same distribution?


     uvec Ns;
     uvec j_uvec(1);
     vec Csi_Nsi;
     rowvec Bsi;
     mat CNsi,ICNsi;
     double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf

    for(int i=0; i<s; ++i){

      // base case when i = 0, N(0) is th empty set
      if(i==0){ 
        condMean = 0;
        Fsi = sigma2_z;
        // empty set, the first point comes from an indep normal

        // increment the prior
        log_p_z += -p*0.5*log(2*M_PI) -p*0.5*log(Fsi) -0.5 / Fsi * as_scalar( sum( square( z.col(approx_order_prior(i)) - condMean ) ));

      }else{
        int n_Ns = std::min(i,m); // number of points in the neighbours set
        // catch the n_Ns neighbours
        Ns = conv_to<uvec>::from( Nsets_prior.submat( i, 0, i, n_Ns-1 ) );

        // compute quantities
        Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
        for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_prior, sigma2_z,phi_z,Dx(Ns(j),approx_order_prior(i)));

        CNsi = matern(ni_prior,sigma2_z,phi_z,Dx.submat(Ns,Ns));
        bool check = inv_sympd(ICNsi,CNsi);
        if(!check) return -std::numeric_limits<double>::infinity();//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

        Bsi = trans(Csi_Nsi) * ICNsi ;
        Fsi = sigma2_z - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );

        // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
        if( Fsi <= 0 ) return -DBL_MAX;  //if it fails because of phi_y -> Inf of Simga_y -> 0

        // increment the prior
        for(int j=0; j<p; ++j){
          j_uvec(0) = j;
          log_p_z += -0.5*log(2*M_PI) -0.5*log(Fsi) -0.5 / Fsi * ( pow( z(j,approx_order_prior(i)) - as_scalar(Bsi * trans( z.submat(j_uvec,Ns)) ) ,2) );
        }
      }

    }
    
  }else{
    log_p_s_z = log_p_phi_z = log_p_z = 0.; //otherwise set to a constant so that the it can continue without taking Z into account
  }
  
  // phi_y
  if(phi_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_phi_y = (shape_p_y-1)*log(phi_y) -rate_p_y*phi_y + shape_p_y*log(rate_p_y) - boost::math::lgamma(shape_p_y);
  
  // sigma 2 y
  if(sigma2_y<=0) return -std::numeric_limits<double>::infinity();//DBL_MAX;
  double log_p_s_y = (shape_s_y-1)*log(sigma2_y) -rate_s_y*sigma2_y + shape_s_y*log(rate_s_y) - boost::math::lgamma(shape_s_y);
  
  // Joint prior
  double lp = ( log_p_s_z + log_p_phi_z + log_p_z + log_p_phi_y + log_p_s_y);
  
  return lp;
} //log prior

// ************************ ELL SS

mat conditional_NN_elliptical_ss(mat Z,double &ll,mat Dx, double sigma2_z, double phi_z, mat y,int n,mat X,double sigma2_y,double phi_y,double temp, uvec approx_order_prior, imat Nsets_prior, double &count){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter

  //Choose the Ellipse(s)
  mat nu(p,s);
  mat z_next = Z;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found,check;

  // Compute the covariance matrix for Z
  // cube Sigma = cube(s,s,p);  // each Sigma_p will be slightly different based onthe distance structure? NO! Coz they're all based on Dx which is immutable
  // mat Sigma = matern(ni_prior,sigma2_z,phi_z,Dx); // zeros<mat>(s,s);
  mat Sigma;

  /* computing prior variance *******************************/

  uvec Ns;
  uvec j_uvec(1);
  vec Csi_Nsi;
  rowvec Bsi;
  mat CNsi,ICNsi;
  double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf see pag 31 for the following with convention k is here named s, m is m, q is p

  // note that here we will deal with just univariate processes one at a time
  mat B_S = zeros<mat>(s,s);
  mat F_S = zeros<mat>(s,s);

  for(int i=0; i<s; ++i){

    // base case when i = 0, N(0) is th empty set
    if(i==0){ 
      condMean = 0;
      Fsi = sigma2_z;
      // empty set, the first point comes from an indep normal

      B_S(approx_order_prior(0),approx_order_prior(0)) = 1; //only this element is non-zero in the approx_order_prior(0) row

    }else{

      int n_Ns = std::min(i,m); // number of points in the neighbours set
      // catch the n_Ns neighbours
      Ns = conv_to<uvec>::from( Nsets_prior.submat( i, 0, i, n_Ns-1 ) );

      // compute quantities
      Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
      for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_prior, sigma2_z,phi_z,Dx(Ns(j),approx_order_prior(i)));

      CNsi = matern(ni_prior,sigma2_z,phi_z,Dx.submat(Ns,Ns));
      check = inv_sympd(ICNsi,CNsi);
      if(!check){
        // ll = -std::numeric_limits<double>::infinity(); 
        return Z; 
      }//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

      Bsi = trans(Csi_Nsi) * ICNsi ;  //now this is a rowvec 1xn_Ns

      Fsi = sigma2_z - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );

      // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
      if( Fsi <= 0 ){
        // ll = -std::numeric_limits<double>::infinity(); 
        return Z; 
      }  //if it fails because of phi_y -> Inf or Simga_y -> 0

      // Now put the elements of Bsi into B_S
      B_S(approx_order_prior(i),approx_order_prior(i)) = 1; //diagonal

    for(int j=0; j<n_Ns; ++j) B_S(approx_order_prior(i),Ns(j)) = -Bsi(j); // i,Ns(j) coz i want it lower-triangular since Ns(j) < i for every j by construction

    }

    F_S(approx_order_prior(i),approx_order_prior(i)) = Fsi; 

  }

  check=inv_sympd(Sigma,trans(B_S)* inv(diagmat(F_S)) *B_S);
  if(!check){
    // ll = -std::numeric_limits<double>::infinity(); 
    std::cout << ".";
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  

  /* END prior *******************************/

  // Compute the cholewski and sample from the prior
  mat chol_Sigma;
  check = chol(chol_Sigma,Sigma); // THIS IS STILL S^3... TODO CHECK
  if(!check){
    // ll = -std::numeric_limits<double>::infinity(); 
    std::cout << ".";
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  
  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock

  for(int i=0; i<p; ++i){
    nu.row(i) = normal_01(s).t() * chol_Sigma;   // THIS is still S^2 though! how can I avoid it? Maybe something based on the reduced model? But it woudn't come from the prior in that case!
  }

  omp_unset_lock(&RNGlock); // ************************ lock


  // Then for each Z, run Ell SS

  for(int i=0; i<p; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above

    omp_set_lock(&RNGlock); // ************************ lock

    //ll threshold
    threshold = ll + log(random_01(rng));  

    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;

    omp_unset_lock(&RNGlock); // ************************ lock

    found = false;
    
    while(!found){

      ++count;
      
      z_next.row(i) = Z.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = conditional_t_NN_lL(y,X,z_next,sigma2_y,phi_y,temp);

      if( !std::isfinite(ll_next) ){
        //Z and ll unchanged
        found = true;
      } 

      if(!found){ // to avoid comparisons with NAs
      
        if( ll_next >= threshold ){
          
          Z.row(i) = z_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;

          omp_set_lock(&RNGlock); // ************************ lock
            theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
  
  }

  return Z;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}


mat NN_elliptical_ss(mat Z,double &ll,mat Dx, double sigma2_z, double phi_z, mat y,int n,mat X,double sigma2_y,double phi_y,double temp,uvec approx_order, uvec approx_order_prior, imat Nsets, imat Nsets_prior, double &count){ //by default temp=1, so that MCMC runs on the untempered likelihood
  
  int s = Z.n_cols;
  int p = Z.n_rows;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter

  //Choose the Ellipse(s)
  mat nu(p,s);
  mat z_next = Z;
  
  double threshold, theta, theta_min, theta_max, ll_next;
  double delta; //control purposes
  bool found,check;

  // Compute the covariance matrix for Z
  // cube Sigma = cube(s,s,p);  // each Sigma_p will be slightly different based onthe distance structure? NO! Coz they're all based on Dx which is immutable
  // mat Sigma = matern(ni_prior,sigma2_z,phi_z,Dx); // zeros<mat>(s,s);
  mat Sigma;

  /* computing prior variance *******************************/

  uvec Ns;
  uvec j_uvec(1);
  vec Csi_Nsi;
  rowvec Bsi;
  mat CNsi,ICNsi;
  double Fsi, condMean; //name convention from Banerjee 2016 -- http://arxiv.org/pdf/1406.7343v2.pdf see pag 31 for the following with convention k is here named s, m is m, q is p

  // note that here we will deal with just univariate processes one at a time
  mat B_S = zeros<mat>(s,s);
  mat F_S = zeros<mat>(s,s);

  for(int i=0; i<s; ++i){

    // base case when i = 0, N(0) is th empty set
    if(i==0){ 
      condMean = 0;
      Fsi = sigma2_z;
      // empty set, the first point comes from an indep normal

      B_S(approx_order_prior(0),approx_order_prior(0)) = 1; //only this element is non-zero in the approx_order_prior(0) row

    }else{

      int n_Ns = std::min(i,m); // number of points in the neighbours set
      // catch the n_Ns neighbours
      Ns = conv_to<uvec>::from( Nsets_prior.submat( i, 0, i, n_Ns-1 ) );

      // compute quantities
      Csi_Nsi = vec(n_Ns); CNsi = mat(n_Ns,n_Ns);
      for(int j=0; j<n_Ns; ++j) Csi_Nsi(j) = matern_point(ni_prior, sigma2_z,phi_z,Dx(Ns(j),approx_order_prior(i)));

      CNsi = matern(ni_prior,sigma2_z,phi_z,Dx.submat(Ns,Ns));
      check = inv_sympd(ICNsi,CNsi);
      if(!check){
        // ll = -std::numeric_limits<double>::infinity(); 
        return Z; 
      }//DBL_MAX;  //if it fails because of phi_z -> Inf of Simga_z -> 0

      Bsi = trans(Csi_Nsi) * ICNsi ;  //now this is a rowvec 1xn_Ns

      Fsi = sigma2_z - as_scalar( trans(Csi_Nsi) * ICNsi * Csi_Nsi );

      // checks on the variance, might go towards strange limits if phi_y -> Inf of Simga_y -> 0
      if( Fsi <= 0 ){
        // ll = -std::numeric_limits<double>::infinity(); 
        return Z; 
      }  //if it fails because of phi_y -> Inf or Simga_y -> 0

      // Now put the elements of Bsi into B_S
      B_S(approx_order_prior(i),approx_order_prior(i)) = 1; //diagonal

    for(int j=0; j<n_Ns; ++j) B_S(approx_order_prior(i),Ns(j)) = -Bsi(j); // i,Ns(j) coz i want it lower-triangular since Ns(j) < i for every j by construction

    }

    F_S(approx_order_prior(i),approx_order_prior(i)) = Fsi; 

  }

  check=inv_sympd(Sigma,trans(B_S)* inv(diagmat(F_S)) *B_S);
  if(!check){
    // ll = -std::numeric_limits<double>::infinity(); 
    std::cout << ".";
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  

  /* END prior *******************************/

  // Compute the cholewski and sample from the prior
  mat chol_Sigma;
  check = chol(chol_Sigma,Sigma); // THIS IS STILL S^3... TODO CHECK
  if(!check){
    // ll = -std::numeric_limits<double>::infinity(); 
    std::cout << ".";
    return Z; 
  } // if the decomp fails because of either phi or sigma   _z, return the current value (it'll get assigned a 0 likelihood and not being considered after..)
  
  // Sample the new ellipse(s)
  omp_set_lock(&RNGlock); // ************************ lock

  for(int i=0; i<p; ++i){
    nu.row(i) = normal_01(s).t() * chol_Sigma;   // THIS is still S^2 though! how can I avoid it? Maybe something based on the reduced model? But it woudn't come from the prior in that case!
  }

  omp_unset_lock(&RNGlock); // ************************ lock


  // Then for each Z, run Ell SS

  for(int i=0; i<p; ++i){                                        // so this is now basically a gibbs style update ... would be interesting to see if there's a mv varsion of Ell SS to justify the above

    omp_set_lock(&RNGlock); // ************************ lock

    //ll threshold
    threshold = ll + log(random_01(rng));  

    //Draw the first proposal, also defining initial brakets
    theta = random_01(rng)*2*M_PI;
    theta_min = theta - 2*M_PI;
    theta_max = theta;
    delta = theta_max-theta_min;


    omp_unset_lock(&RNGlock); // ************************ lock

    found = false;
    
    while(!found){

      count++;
      
      z_next.row(i) = Z.row(i)*cos(theta) + nu.row(i)*sin(theta);    // this and the following can be done coz I'm supposing the cross-covariance to be diagonal, hence each conditional is just the marginal!
      
      // -- log Likelihood
      ll_next = t_NN_lL(y,X,z_next,sigma2_y,phi_y,temp,approx_order,Nsets);

      if( !std::isfinite(ll_next) ){
        //Z and ll unchanged
        found = true;
      } 

      if(!found){ // to avoid comparisons with NAs
      
        if( ll_next >= threshold ){
          
          Z.row(i) = z_next.row(i);
          ll = ll_next; //this should update even the external ll
          found = true;
          
        }else{
          
          if(theta < 0){theta_min = theta;}else{theta_max = theta;}
          
          delta = theta_max-theta_min;

          omp_set_lock(&RNGlock); // ************************ lock
            theta = random_01(rng)*(theta_max-theta_min)+theta_min;
          omp_unset_lock(&RNGlock); // ************************ lock
        }
        
        if( delta < 1.e-10){ //is it needed? well otherwise sometimes it just gets stuck...let us see the results..
          break;
        }
      }
      
    }
  
  }

  return Z;  // note I could update Z passing it by reference, but in case the likelihood is found to be nan/NA, I would not have any fallback for now
  
}
// NOW, baseline model and MCMC are not implemented (not really needed)

/* #########################################################################
####################    Dim Exp  SMC fixed p     ###########################
############################################################################ */


Rcpp::List NN_DE_SMC(int n_part, mat X, mat y, int n,int p, double factor_temp ,double thresh, int det_schedule, int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                  double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter

  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper_Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  
  cube Z = zeros<cube>(p,s,n_part);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part); vec lsigma2_z(n_part);vec lp_z(n_part);
  mat theta(n_part,4); mat theta_new(n_part,4);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  
  // cube post_Dxz(s,s,n_part);
  // cube post_Exz(s,s,n_part);
  
  vec lrw = zeros(n_part);
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  double tmp_ess; double tmp_cess;

  vec essCount(n);

  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  uvec n_idx = zeros<uvec>(n_part);
  
  double sum_lw;
  
  
  mat tmpMat(n_part,s);
  vec tmpVec(n_part);
  mat tmp_Ex(s,s); mat c_Ex(s,s);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(4,4);;
  mat chol_Sigma_prop = eye<mat>(4,4);;
  mat old_Sigma_prop = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);

  vec tmp_eigval;
  mat tmp_eigvec;
  
  bool check;

  int i_overlap; int iterator_overlap; int n_overlap=2; // NB this number is a parameter, we might wish to tune it

  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // silly cout
  std::cout << "Threshold for resampling: ESS <= " << thresh << std::endl;

  // Sim from the prior
  std::cout << "Sim from the prior ... "; std::cout.flush();
  
  mat Dx = distMat(X);
  
  // DISCLAMER: IF p==0 Z, sigma_z and phi_z are useless, but as they do not impact ll and lp I will keep them despite the computation loss
  // TODO REMOVE IT, especially the Zs as they impact A LOT the computing time (as for now, I will just remove the EllSliceSamp)
  
  // sigma_z & phi_z
  lsigma2_z = log(random_gamma(n_part,sigma_z_prior)); //lsigma2_z = log(1.) + normal_01(n_part)/500.; //DEBUG
  lp_z = log(random_gamma(n_part,phi_z_prior)); //lp_z = log(0.5) + normal_01(n_part)/500.; //DEBUG
  
  // Z
  cube tmp_Z= zeros<cube>(p,s,n_part) ;

  #pragma omp parallel for private(tmp_Ex,c_Ex)
  for(int j=0; j<n_part; ++j){

    tmp_Ex = matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx);
    check = chol(c_Ex,tmp_Ex);

    if(check){
      for(int k=0; k<p; ++k){
  
        omp_set_lock(&RNGlock); // ************************ lock
          Z.slice(j).row(k) = normal_01(s).t(); // chol(tmp_Ex) * random std normals
        omp_unset_lock(&RNGlock); // ************************ lock
  
      }
  
      Z.slice(j) = Z.slice(j) * c_Ex; 

    }else{
      // do not rescale, it will get erased (ll=-INf) when computing the likelihood...
    }


  }

  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
    if(std::isnan(lsigma2_z(j)) || !std::isfinite(lsigma2_z(j))) lsigma2_z(j)=-DBL_MAX;
    if(std::isnan(lp_z(j)) || !std::isfinite(lp_z(j))) lp_z(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  old_Sigma_prop(2,2) = w_var(lsigma2_z,W);
  old_Sigma_prop(3,3) = w_var(lp_z,W);
  

  mat Sigma_prop_0 = old_Sigma_prop;
  
    // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) PRIOR SIDE (Zs)
	uvec approx_order_prior = linspace<uvec>(0,s-1,s);
	int n_centroids_prior = 3;

	  // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) LIKELIHOOD SIDE
	uvec approx_order = linspace<uvec>(0,s-1,s);
  uvec old_approx_order = approx_order;
	vec mean_dist(s);
	vec median_dist(s);
	mat Dxz_tmp(s,s);
	uvec chosen_idx(s);
	uvec free_idx(s);
	int n_centroids = 3;  //TODO, set to a meaningful value

	uvec tmp_vec_idx;
    uvec tmp_single_idx;
    int tmp_idx;

	  // **********************
    // *** START the model approx now
    // **********************
    
	// choose the starting point(s)
	median_dist = median(Dx).t();
	mean_dist = mean(Dx).t();

	tmp_vec_idx = stable_sort_index( median_dist, "descend" ); // if "ascend" gets the one with least distance, with "descend" get the most distants 

	approx_order_prior.subvec(0,n_centroids_prior-1) = tmp_vec_idx.subvec(0,n_centroids_prior-1); 
	
	// set starting indexes
	chosen_idx.fill(s); 
	chosen_idx.subvec(0,n_centroids_prior-1) = approx_order_prior.subvec(0,n_centroids_prior-1); 
	free_idx = linspace<uvec>(0,s-1,s);

	for(int j=0; j<n_centroids_prior; ++j){
		tmp_single_idx = find(free_idx == chosen_idx(j),1); //Messy, can we do it better?
	    tmp_idx = tmp_single_idx(0);
	    free_idx.shed_row( tmp_idx );
	} 

	// then get the others
	for(int j=n_centroids_prior; j<s; ++j){

		//get distances of all the remaining points wrt the already selected ones
		mean_dist = mean(Dx.submat( chosen_idx.subvec(0,j-1) , free_idx )).t();
		tmp_single_idx = stable_sort_index(mean_dist, "descend");
		tmp_idx = tmp_single_idx(0);

		approx_order_prior(j) = free_idx(tmp_idx);
	    chosen_idx(j) = approx_order_prior(j);
	    
		free_idx.shed_row( tmp_idx );
	}

	//check DEBUG
	//check if this vector goes from 0 to n_part-1
	if( sum(approx_order_prior) != ((double)s*(s-1.)/2.) ) std::cout << std::endl<< "WRONG INDEX SUM!" << std::endl;


  imat Nsets_prior = imat(s,m); Nsets_prior.fill(-1);
  uvec Ns_prior(s); uvec tmp_uvec;

  for(int j = 1; j<s; ++j){
    
    Ns_prior = stable_sort_index( Dx.submat( approx_order_prior.subvec(j,j) , approx_order_prior.subvec(0,j-1) ) , "ascend");
    tmp_uvec = approx_order_prior.subvec(0,j-1);
    Ns_prior = tmp_uvec.elem(Ns_prior);

    for(int k=0; k< std::min(j,m); ++k){
      Nsets_prior(j,k) = Ns_prior(k);
    }
  }

	// END MODEL APPROX


  // Likelihood approx model
  approx_order = approx_order_prior; //for the start

  imat Nsets = imat(s,m); Nsets.fill(-1);
  imat old_Nsets = Nsets;
  uvec Ns(s);

  for(int j = 1; j<s; ++j){
    
    Ns = stable_sort_index( Dx.submat( approx_order.subvec(j,j) , approx_order.subvec(0,j-1) ) , "ascend");
    tmp_uvec = approx_order.subvec(0,j-1);
    Ns = tmp_uvec.elem(Ns);

    for(int k=0; k< std::min(j,m); ++k){
      Nsets(j,k) = Ns(k);
    }
  }

  // end likelihood model

  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment, but I compute it to keep track of it)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = NN_lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
    ll_curr(j) = t_NN_lL(y,X,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),temp(i),approx_order,Nsets);
  }

  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    // increment iteration count
    i++;
    
    if(i >= temp.n_elem){ 
      temp.resize(2*temp.n_elem);
      essCount.resize(2*essCount.n_elem);
    }

    
    cout << std::endl << "######## Iteration " << i << " " << endl;

    ll_tmp = ll_curr; //starting point for ll_tmp
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, totally worthless just placeholder!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }
    
    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    // ***** Weights ********
    
    /*  math
              lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
            lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh /*|| temp(i) ==1*/ ){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = resid_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      
      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      cess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    

    ///////////////////////////////////////////////////////////////////

    // **********************
    // *** Control and modify the model approx now
    // **********************

    std::cout << "Starting adaptation wrt the order considered in NN ...";

    old_approx_order = approx_order;
    old_Nsets = Nsets;

    Dxz_tmp = distMat(join_rows(X,trans(Z.slice(0))));
    for(int j=1; j<n_part; ++j) Dxz_tmp = Dxz_tmp + distMat(join_rows(X,trans(Z.slice(1))));
    Dxz_tmp = Dxz_tmp/(double)n_part;
      
    // choose the starting point(s)
    median_dist = median(Dxz_tmp).t();
    mean_dist = mean(Dxz_tmp).t();

    tmp_vec_idx = stable_sort_index( median_dist, "ascend" ); // if "ascend" gets the one with least distance, with "descend" get the most distants 

    // set starting indexes
    // *** approx_order.subvec(0,n_centroids-1) = tmp_vec_idx.subvec(0,n_centroids-1);

    chosen_idx.fill(s); chosen_idx.subvec(0,n_centroids-1) = approx_order.subvec(0,n_centroids-1);
    free_idx = linspace<uvec>(0,s-1,s);
    
    for(int j=0; j<n_centroids; ++j){
      tmp_single_idx = find(free_idx == chosen_idx(j),1);
      tmp_idx = tmp_single_idx(0);
      free_idx.shed_row( tmp_idx );
    } 

    // *** THIS WAY STARTING POINTS/CENTROIDS ARE THE SAME, BUT I CHANGE THE REST

    // then get the others
    for(int j=n_centroids; j<s; ++j){
      
      //get distances of all the remaining points wrt the already selected ones
      mean_dist = mean(Dxz_tmp.submat( chosen_idx(span(0,j-1)) , free_idx )).t();

      tmp_single_idx = stable_sort_index(mean_dist, "ascend");
      tmp_idx = tmp_single_idx(0);
      approx_order(j) = free_idx(tmp_idx);
      chosen_idx(j) = approx_order(j);
      
      free_idx.shed_row( tmp_idx );
    }

    //check DEBUG
    //check if this vector goes from 0 to n_part-1
    if( sum(approx_order) != ((double)s*(s-1.)/2.) ) std::cout << std::endl<< "WRONG INDEX SUM!"<< std::endl;

    Nsets.fill(-1);
    for(int j = 1; j<s; ++j){
      
      Ns = stable_sort_index( Dxz_tmp.submat( approx_order.subvec(j,j) , approx_order.subvec(0,j-1) ) , "ascend");
      tmp_uvec = approx_order.subvec(0,j-1);
      Ns = tmp_uvec.elem(Ns);

      for(int k=0; k< std::min(j,m); ++k){
        Nsets(j,k) = Ns(k);
      }
    }

    std::cout << " Reweighting ...";

    //recompute the likelihoods wrt this new ordering
    #pragma omp parallel for default(shared)
    for(int j=0; j<n_part; ++j){
      ll_tmp(j) = t_NN_lL(y,X,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),temp(i),approx_order,Nsets);
    }

    //recompute ess wrt new ordering
    lrw = ( ll_tmp - ll_curr) ; //note that the prior remains the same (coz is done only on Dx which is not changed), hence does not enter in the reweighting

    // ***** ESS ********
    tmp_ess = ESS_log(lW + lrw); // new ESS with the un-Normalized weights!
    tmp_cess = cESS_log( lrw , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    if( tmp_ess <= std::max(10.0,0.001*(double)n_part) ){

      // restore PARTIALLY the old order
      i_overlap = 0;
      iterator_overlap = 0;

      while(i_overlap < n_overlap && iterator_overlap < s){  

        if( old_approx_order(iterator_overlap) != approx_order(iterator_overlap) ){

          tmp_single_idx = find( approx_order == old_approx_order(iterator_overlap) ); // this is the index in the current vec
          
          approx_order(tmp_single_idx(0)) = approx_order(iterator_overlap);
          approx_order(iterator_overlap) = old_approx_order(iterator_overlap); //swap the two indexes

          i_overlap++;
        }
        ++iterator_overlap;
      }

      std::cout << " had to swap ONLY"<< i_overlap << " elements ... ";
      // a fancy way could be computing the kendall tau rank distance and act accordingly, i.e. swapping elements till the distance is reduced.


      Nsets.fill(-1);
      for(int j = 1; j<s; ++j){
        
        Ns = stable_sort_index( Dxz_tmp.submat( approx_order.subvec(j,j) , approx_order.subvec(0,j-1) ) , "ascend");
        tmp_uvec = approx_order.subvec(0,j-1);
        Ns = tmp_uvec.elem(Ns);
      
        for(int k=0; k< std::min(j,m); ++k){
          Nsets(j,k) = Ns(k);
        }
      }

      //recompute the likelihoods wrt this new ordering
      #pragma omp parallel for default(shared)
      for(int j=0; j<n_part; ++j){
        ll_tmp(j) = t_NN_lL(y,X,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),temp(i),approx_order,Nsets);
      }

      //recompute ess wrt new ordering
      lrw = ( ll_tmp - ll_curr) ; //note that the prior remains the same (coz is done only on Dx which is not changed), hence does not enter in the reweighting

      // ***** ESS ********
      tmp_ess = ESS_log(lW + lrw); 
      tmp_cess = cESS_log( lrw , lW); 

      if( tmp_ess <= std::max(10.0,0.001*(double)n_part) ){
        approx_order = old_approx_order;
        Nsets = old_Nsets;
        ll_tmp=ll_curr;
        lrw = ( ll_tmp - ll_curr) ;
        tmp_ess = ess; 
        tmp_cess = cess; 
      }

    }

    ess = tmp_ess;
    cess = tmp_cess;

    // Compute and Normalize the (LOG_)WEIGHTS
    lw = lW + lrw;
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    ll_curr = ll_tmp;

    std::cout << " ESS endpoint = " << ess << " "; 
    std::cout << " Done!" << endl;
    // END ORDERING


  // Now I need to protect myself from models varying too hard when changing orders for the approx
    if(ess < thresh ){ 
      
      std::cout << "Resampling because of the ordering ... " ;  
      
      idx = resid_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      
      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      cess = n_part;
      
      std::cout << "Resampled! "  << std::endl;  
      
      //adjusting some indexes and variables
      ll_tmp = ll_curr; //starting point for ll_tmp

  } //end IF I did resample because of the ordering


    //////////////////////////////////////////////////////////////////

    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96, would be 2.38 something...actually a bit less for the optimal acc rate in dim 4 is less then 0.234
    // in our case though the variance is computed on a set that comes from a previous iter, even though is actually resampled to get to the current temp
    // so maybe a bit less would make sense...

   // weighted covariance matrix too slow.
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(2,2) = w_var(lsigma2_z,W);
    Sigma_prop(3,3) = w_var(lp_z,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(0,2) = w_cov(lsigma2_y,lsigma2_z,W);
    Sigma_prop(0,3) = w_cov(lsigma2_y,lp_z,W);
    
    Sigma_prop(1,2) = w_cov(lp_y,lsigma2_z,W);
    Sigma_prop(1,3) = w_cov(lp_y,lp_z,W);
    
    Sigma_prop(2,3) = w_cov(lsigma2_z,lp_z,W);
    
    Sigma_prop(1,0) = Sigma_prop(0,1);
    Sigma_prop(2,0) = Sigma_prop(0,2);
    Sigma_prop(3,0) = Sigma_prop(0,3);
    Sigma_prop(2,1) = Sigma_prop(1,2);
    Sigma_prop(3,1) = Sigma_prop(1,3);
    Sigma_prop(3,2) = Sigma_prop(2,3);
    
    // unweighted, but if we just resampled...then find nearest valid!
    theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
    // Sigma_prop = cov(theta);

    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck; 
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;


    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      std::cout << " !! ";                        // find nearest covariance matrix to the one fond there TODO
      
      //TODO better, this is just a patch
      eig_sym(tmp_eigval,tmp_eigvec,Sigma_prop,"std");
      for(int ii=0; ii<4; ++ii){
        if( tmp_eigval(ii) <= 0) tmp_eigval(ii) = 1e-5;
      }
      Sigma_prop = tmp_eigvec * diagmat(tmp_eigval) * trans(tmp_eigvec);      

      if( !chol(chol_Sigma_prop,Sigma_prop) ){  // ... never lucky
        std::cout << " ?? ";                       
        Sigma_prop = old_Sigma_prop;
        chol(chol_Sigma_prop,Sigma_prop);
      }
    }      
  
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*4);
      tmp_mat.resize(n_part,4);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      // theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z); SUPERFLUOUS
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = t_NN_lL(y,X,Z.slice(j),exp(theta_new(j,0)),exp(theta_new(j,1)),temp(i),approx_order,Nsets);
        lprior_new(j) = NN_lp(Z.slice(j),p,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
      }
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)) + (theta_new(j,2) - theta(j,2)) + (theta_new(j,3) - theta(j,3)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      n_idx = find(log(rand) >= acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);

      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);

      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      lsigma2_z = theta.col(2);
      lp_z = theta.col(3);

      
      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  

      //weights stay the same
      
    } //end multiple MCMC steps
    
    if(p>0){

      //******************************************
      //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
      //******************************************

      essCount(i) = 0;
      
      for(int jj=0;jj<J;jj++){ // for more then one step...
        
        std::cout << "Ell_SS step #" <<jj+1 << " ... ";  
        
        #pragma omp parallel for
        for(int j=0; j<n_part; ++j){ //every particle
          
          Z.slice(j) = NN_elliptical_ss( Z.slice(j) , ll_curr(j), Dx, exp(lsigma2_z(j)) , exp(lp_z(j)) ,y,n,X, exp(lsigma2_y(j)),exp(lp_y(j)) , temp(i), approx_order,approx_order_prior,Nsets,Nsets_prior, essCount(i));
          
          // Note that ll_curr is updated by passing it by reference (I could actually do the same for Z)
          // lprior needs to be updated though 
          lprior_curr(j) = NN_lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
          
        } // loop for every particle
      } // loop J times EllSliceSsamp
      
      essCount(i) = ((essCount(i) / n_part) / J )/ p ;
      std::cout << "(avg#steps " << essCount(i) << ") -- Done!" << endl;

      
      // Note there is no need to zero out the rest as for now the dimension is fixed
      
    } // if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same
    
    
    //  ********* END full-MCMC MOVE
    

    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_part; ++j){
  //   post_Dxz.slice(j) = distMat(join_rows(X,trans(Z.slice(j))));
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j)); //cant I just translate X at the start?
  // }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
   
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);
  essCount.resize(i);

  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,trans(Z.slice(j))))); 
  }
  post_mean_Exz = post_mean_Exz/(double)n_part;

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
  												  Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=p,Rcpp::Named("Z")=Z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=lp_z, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp, Rcpp::Named("avg_ess_cout") = mean(essCount));
  return out;
  
}

Rcpp::List baseline_NN_DE_SMC(int n_part, mat X, mat y, int n,int p, double factor_temp ,double thresh, int det_schedule, int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter

  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part);
  mat theta(n_part,2); mat theta_new(n_part,2);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  
  // cube post_Dxz(s,s,n_part);
  // cube post_Exz(s,s,n_part);
  
  vec lrw = zeros(n_part);
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  double tmp_ess; double tmp_cess;

  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  uvec n_idx = zeros<uvec>(n_part);
  
  double sum_lw;
  
  
  mat tmpMat(n_part,s);
  vec tmpVec(n_part);
  mat tmp_Ex(s,s); mat c_Ex(s,s);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(2,2);;
  mat chol_Sigma_prop = eye<mat>(2,2);;
  mat old_Sigma_prop = eye<mat>(2,2);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);

  vec tmp_eigval;
  mat tmp_eigvec;
  
  bool check;

  int i_overlap; int iterator_overlap; int n_overlap=2; // NB this number is a parameter, we might wish to tune it

  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // silly cout
  std::cout << "Threshold for resampling: ESS <= " << thresh << std::endl;

  // Sim from the prior
  std::cout << "Sim from the prior ... "; std::cout.flush();
  
  mat Dx = distMat(X);
  
    // Dummy zeta vars
  mat dummy_z = zeros<mat>(1,s);
  double dummy_p_z = 1.; double dummy_s_z = 1.;
  double shape_p_z = 1.; double rate_p_z = 1.;double shape_s_z = 1.;double rate_s_z = 1.;

  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);

  mat Sigma_prop_0 = old_Sigma_prop;
  
  // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) PRIOR SIDE (Zs)
  uvec approx_order_prior = linspace<uvec>(0,s-1,s);
  int n_centroids_prior = 3; //could do it with kmeans but...

  // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) LIKELIHOOD SIDE
  uvec approx_order = linspace<uvec>(0,s-1,s);
  uvec old_approx_order = approx_order;
  vec mean_dist(s);
  vec median_dist(s);
  mat Dxz_tmp(s,s);
  uvec chosen_idx(s);
  uvec free_idx(s);
  int n_centroids = 3;  //TODO, set to a meaningful value

  uvec tmp_vec_idx;
    uvec tmp_single_idx;
    int tmp_idx;

    // **********************
    // *** START the model approx now
    // **********************
    
  // choose the starting point(s)
  median_dist = median(Dx).t();
  mean_dist = mean(Dx).t();

  tmp_vec_idx = stable_sort_index( median_dist, "descend" ); // if "ascend" gets the one with least distance, with "descend" get the most distants 

  approx_order_prior.subvec(0,n_centroids_prior-1) = tmp_vec_idx.subvec(0,n_centroids_prior-1); 
  
  // set starting indexes
  chosen_idx.fill(s); 
  chosen_idx.subvec(0,n_centroids_prior-1) = approx_order_prior.subvec(0,n_centroids_prior-1); 
  free_idx = linspace<uvec>(0,s-1,s);

  for(int j=0; j<n_centroids_prior; ++j){
    tmp_single_idx = find(free_idx == chosen_idx(j),1); //Messy, can we do it better?
      tmp_idx = tmp_single_idx(0);
      free_idx.shed_row( tmp_idx );
  } 

  // then get the others
  for(int j=n_centroids_prior; j<s; ++j){

    //get distances of all the remaining points wrt the already selected ones
    mean_dist = mean(Dx.submat( chosen_idx.subvec(0,j-1) , free_idx )).t();
    tmp_single_idx = stable_sort_index(mean_dist, "descend");
    tmp_idx = tmp_single_idx(0);

    approx_order_prior(j) = free_idx(tmp_idx);
      chosen_idx(j) = approx_order_prior(j);
      
    free_idx.shed_row( tmp_idx );
  }

  //check DEBUG
  //check if this vector goes from 0 to n_part-1
  if( sum(approx_order_prior) != ((double)s*(s-1.)/2.) ) std::cout << std::endl<< "WRONG INDEX SUM!" << std::endl;


  imat Nsets_prior = imat(s,m); Nsets_prior.fill(-1);
  uvec Ns_prior(s); uvec tmp_uvec;

  for(int j = 1; j<s; ++j){
    
    Ns_prior = stable_sort_index( Dx.submat( approx_order_prior.subvec(j,j) , approx_order_prior.subvec(0,j-1) ) , "ascend");
    tmp_uvec = approx_order_prior.subvec(0,j-1);
    Ns_prior = tmp_uvec.elem(Ns_prior);

    for(int k=0; k< std::min(j,m); ++k){
      Nsets_prior(j,k) = Ns_prior(k);
    }
  }

  // END prior MODEL APPROX


  // Likelihood approx model
  approx_order = approx_order_prior; //for the start

  imat Nsets = imat(s,m); Nsets.fill(-1);
  imat old_Nsets = Nsets;
  uvec Ns(s);

  for(int j = 1; j<s; ++j){
    
    Ns = stable_sort_index( Dx.submat( approx_order.subvec(j,j) , approx_order.subvec(0,j-1) ) , "ascend");
    tmp_uvec = approx_order.subvec(0,j-1);
    Ns = tmp_uvec.elem(Ns);

    for(int k=0; k< std::min(j,m); ++k){
      Nsets(j,k) = Ns(k);
    }
  }

  // end likelihood model

  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment, but I compute it to keep track of it)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = NN_lp(dummy_z,p,exp(lsigma2_y(j)),exp(lp_y(j)),dummy_s_z,dummy_p_z,shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
    ll_curr(j) = t_NN_lL(y,X,dummy_z,exp(lsigma2_y(j)),exp(lp_y(j)),temp(i),approx_order,Nsets);
  }

  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    // increment iteration count
    i++;
    if(i >= temp.n_elem) temp.resize(2*temp.n_elem); //resise temp vec if needed
    
    cout << std::endl << "######## Iteration " << i << " " << endl;


    ll_tmp = ll_curr; //starting point for ll_tmp
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, totally worthless just placeholder!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }
    
    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    // ***** Weights ********
    
    /*  math
    lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
    lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh /*|| temp(i) ==1*/ ){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = resid_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      cess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96, would be 2.38 something...actually a bit less for the optimal acc rate in dim 4 is less then 0.234
    // in our case though the variance is computed on a set that comes from a previous iter, even though is actually resampled to get to the current temp
    // so maybe a bit less would make sense...

   // weighted covariance matrix too slow.
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(1,0) = Sigma_prop(0,1);
    
    // unweighted, but if we just resampled...then find nearest valid!
    theta = join_horiz(lsigma2_y,lp_y);
    // Sigma_prop = cov(theta);

    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck; 
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;


    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      std::cout << " !! ";                        // find nearest covariance matrix to the one fond there TODO
      
      //TODO better, this is just a patch
      eig_sym(tmp_eigval,tmp_eigvec,Sigma_prop,"std");
      for(int ii=0; ii<2; ++ii){
        if( tmp_eigval(ii) <= 0) tmp_eigval(ii) = 1e-5;
      }
      Sigma_prop = tmp_eigvec * diagmat(tmp_eigval) * trans(tmp_eigvec);      

      if( !chol(chol_Sigma_prop,Sigma_prop) ){  // ... never lucky
        std::cout << " ?? ";                       
        Sigma_prop = old_Sigma_prop;
        chol(chol_Sigma_prop,Sigma_prop);
      }
    }      
  
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*2);
      tmp_mat.resize(n_part,2);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      // theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z); SUPERFLUOUS
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = t_NN_lL(y,X,dummy_z,exp(theta_new(j,0)),exp(theta_new(j,1)),temp(i),approx_order,Nsets);
        lprior_new(j) = NN_lp(dummy_z,p,exp(theta_new(j,0)),exp(theta_new(j,1)),dummy_s_z,dummy_p_z,
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
      }
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      n_idx = find(log(rand) >= acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);

      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);

      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);

      
      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  

      //weights stay the same
      
    } //end multiple MCMC steps
    
    //  ********* END full-MCMC MOVE
  
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_part; ++j){
  //   post_Dxz.slice(j) = distMat(join_rows(X,trans(Z.slice(j))));
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j)); //cant I just translate X at the start?
  // }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
   
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);

  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,dummy_z))); 
  }
  post_mean_Exz = post_mean_Exz/(double)n_part;

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
                            Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=0,Rcpp::Named("Z")=dummy_z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=1.,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=1., Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp);
  return out;
  
}

Rcpp::List conditional_NN_DE_SMC(int n_part, mat X, mat y, int n,int p, double factor_temp ,double thresh, int det_schedule, int M, int J,
                  double shape_p_y, double rate_p_y,double shape_s_y, double rate_s_y,
                  double shape_p_z, double rate_p_z,double shape_s_z, double rate_s_z)
{
  
  omp_init_lock(&RNGlock);  
  rng.seed(time(0));
  
  
  int s = X.n_rows;
  int d = X.n_cols;
  int m = std::min(GLOBAL_M, s-1); // number of neighbours to consider at max. Could be a parameter

  // ***************************
  // Initialization
  // ***************************
  
  double factor_sd;
  double m_p,sd_p,m_s,sd_s;
  mat E_hat(s,s);
  
  
  int i=0;
  
  
  // Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_y_prior(shape_s_y, 1./rate_s_y); 
  
  //Prior for the Phi s
  boost::random::gamma_distribution<> phi_y_prior(shape_p_y, 1./rate_p_y); 
  
  
  
  // Hyper-Prior for the Sigma s
  boost::random::gamma_distribution<> sigma_z_prior(shape_s_z, 1./rate_s_z); 
  
  //Hyper_Prior for the Phi s
  boost::random::gamma_distribution<> phi_z_prior(shape_p_z, 1./rate_p_z); 
  
  
  cube Z = zeros<cube>(p,s,n_part);
  // AAA notice how now particles are on the slice, and each slice has p rows (so each row an indep Z_i)
  
  
  vec lsigma2_y(n_part); vec lp_y(n_part); vec lsigma2_z(n_part);vec lp_z(n_part);
  mat theta(n_part,4); mat theta_new(n_part,4);
  
  vec p_new(n_part); mat tmp_p(n_part,2);
  vec tmp_sz(n_part); vec tmp_pz(n_part);
  
  vec ll_curr(n_part);
  vec ll_new(n_part);
  
  vec lprior_curr(n_part);
  vec lprior_new(n_part);
  
  
  // cube post_Dxz(s,s,n_part);
  // cube post_Exz(s,s,n_part);
  
  vec lrw = zeros(n_part);
  vec lw = zeros(n_part); lw.fill(0.); // log
  vec lW(n_part); lW.fill(log(1./n_part)); //log normalized
  vec W(n_part); W.fill(1./n_part);//normalized
  
  vec temp(n);     // Temperature ladder
  double ess; double cess;
  double tmp_ess; double tmp_cess;

  vec essCount(n);

  vec acc_rate(n_part); vec rand(n_part); vec accepted(n_part);
  
  
  // lots of useless variables for resampling
  uvec idx = zeros<uvec>(n_part);
  uvec n_idx = zeros<uvec>(n_part);
  
  double sum_lw;
  
  
  mat tmpMat(n_part,s);
  vec tmpVec(n_part);
  mat tmp_Ex(s,s); mat c_Ex(s,s);
  double sign,log_det_Ex;
  
  //MCMC
  mat Sigma_prop = eye<mat>(4,4);;
  mat chol_Sigma_prop = eye<mat>(4,4);;
  mat old_Sigma_prop = eye<mat>(4,4);
  mat tmp_mat;
  double old_sd_p = 0;
  double old_sd_s = 0;
  
  vec tmp_vec(s+1);

  vec tmp_eigval;
  mat tmp_eigvec;
  
  bool check;

  int i_overlap; int iterator_overlap; int n_overlap=2; // NB this number is a parameter, we might wish to tune it

  // ***********************************
  // *********  Step 0  ****************
  // ***********************************
  
  // silly cout
  std::cout << "Threshold for resampling: ESS <= " << thresh << std::endl;

  // Sim from the prior
  std::cout << "Sim from the prior ... "; std::cout.flush();
  
  mat Dx = distMat(X);
  
  // DISCLAMER: IF p==0 Z, sigma_z and phi_z are useless, but as they do not impact ll and lp I will keep them despite the computation loss
  // TODO REMOVE IT, especially the Zs as they impact A LOT the computing time (as for now, I will just remove the EllSliceSamp)
  
  // sigma_z & phi_z
  lsigma2_z = log(random_gamma(n_part,sigma_z_prior)); //lsigma2_z = log(1.) + normal_01(n_part)/500.; //DEBUG
  lp_z = log(random_gamma(n_part,phi_z_prior)); //lp_z = log(0.5) + normal_01(n_part)/500.; //DEBUG
  
  // Z
  cube tmp_Z= zeros<cube>(p,s,n_part) ;

  #pragma omp parallel for private(tmp_Ex,c_Ex)
  for(int j=0; j<n_part; ++j){

    tmp_Ex = matern(ni_prior,exp(lsigma2_z(j)),exp(lp_z(j)),Dx);
    check = chol(c_Ex,tmp_Ex);

    if(check){
      for(int k=0; k<p; ++k){
  
        omp_set_lock(&RNGlock); // ************************ lock
          Z.slice(j).row(k) = normal_01(s).t(); // chol(tmp_Ex) * random std normals
        omp_unset_lock(&RNGlock); // ************************ lock
  
      }
  
      Z.slice(j) = Z.slice(j) * c_Ex; 

    }else{
      // do not rescale, it will get erased (ll=-INf) when computing the likelihood...
    }


  }

  // sigma and phi_y
  lsigma2_y = log(random_gamma(n_part,sigma_y_prior)); //lsigma2_y = log(5.) + normal_01(n_part)/500.; //DEBUG
  lp_y = log(random_gamma(n_part,phi_y_prior)); //lp_y = log(0.5) + normal_01(n_part)/500.; //DEBUG
  //note that here the convention is shape & scale!!!
  
  for(int j=0;j<n_part;++j){ //other function will take care of it after...
    if(std::isnan(lsigma2_y(j)) || !std::isfinite(lsigma2_y(j))) lsigma2_y(j)=-DBL_MAX;
    if(std::isnan(lp_y(j)) || !std::isfinite(lp_y(j))) lp_y(j)=-DBL_MAX;
    if(std::isnan(lsigma2_z(j)) || !std::isfinite(lsigma2_z(j))) lsigma2_z(j)=-DBL_MAX;
    if(std::isnan(lp_z(j)) || !std::isfinite(lp_z(j))) lp_z(j)=-DBL_MAX;
  }
  
  old_Sigma_prop(0,0) = w_var(lsigma2_y,W);
  old_Sigma_prop(1,1) = w_var(lp_y,W);
  old_Sigma_prop(2,2) = w_var(lsigma2_z,W);
  old_Sigma_prop(3,3) = w_var(lp_z,W);
  

  mat Sigma_prop_0 = old_Sigma_prop;
  
    // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) PRIOR SIDE (Zs)
  uvec approx_order_prior = linspace<uvec>(0,s-1,s);
  int n_centroids_prior = 3;

    // Initialise the 'things' for the model changing wrt to the approx (wrt to the chosen order) LIKELIHOOD SIDE
  uvec approx_order = linspace<uvec>(0,s-1,s);
  uvec old_approx_order = approx_order;
  vec mean_dist(s);
  vec median_dist(s);
  mat Dxz_tmp(s,s);
  uvec chosen_idx(s);
  uvec free_idx(s);
  int n_centroids = 3;  //TODO, set to a meaningful value

  uvec tmp_vec_idx;
    uvec tmp_single_idx;
    int tmp_idx;

  // **********************
  // *** START the model approx for the prior only
  // **********************
    
  // choose the starting point(s)
  median_dist = median(Dx).t();
  mean_dist = mean(Dx).t();

  tmp_vec_idx = stable_sort_index( median_dist, "descend" ); // if "ascend" gets the one with least distance, with "descend" get the most distants 

  approx_order_prior.subvec(0,n_centroids_prior-1) = tmp_vec_idx.subvec(0,n_centroids_prior-1); 
  
  // set starting indexes
  chosen_idx.fill(s); 
  chosen_idx.subvec(0,n_centroids_prior-1) = approx_order_prior.subvec(0,n_centroids_prior-1); 
  free_idx = linspace<uvec>(0,s-1,s);

  for(int j=0; j<n_centroids_prior; ++j){
    tmp_single_idx = find(free_idx == chosen_idx(j),1); //Messy, can we do it better?
      tmp_idx = tmp_single_idx(0);
      free_idx.shed_row( tmp_idx );
  } 

  // then get the others
  for(int j=n_centroids_prior; j<s; ++j){

    //get distances of all the remaining points wrt the already selected ones
    mean_dist = mean(Dx.submat( chosen_idx.subvec(0,j-1) , free_idx )).t();
    tmp_single_idx = stable_sort_index(mean_dist, "descend");
    tmp_idx = tmp_single_idx(0);

    approx_order_prior(j) = free_idx(tmp_idx);
      chosen_idx(j) = approx_order_prior(j);
      
    free_idx.shed_row( tmp_idx );
  }

  //check DEBUG
  //check if this vector goes from 0 to n_part-1
  if( sum(approx_order_prior) != ((double)s*(s-1.)/2.) ) std::cout << std::endl<< "WRONG INDEX SUM!" << std::endl;


  imat Nsets_prior = imat(s,m); Nsets_prior.fill(-1);
  uvec Ns_prior(s); uvec tmp_uvec;

  for(int j = 1; j<s; ++j){
    
    Ns_prior = stable_sort_index( Dx.submat( approx_order_prior.subvec(j,j) , approx_order_prior.subvec(0,j-1) ) , "ascend");
    tmp_uvec = approx_order_prior.subvec(0,j-1);
    Ns_prior = tmp_uvec.elem(Ns_prior);

    for(int k=0; k< std::min(j,m); ++k){
      Nsets_prior(j,k) = Ns_prior(k);
    }
  }

  // END MODEL APPROX

  std::cout << " and initialising wieghts ... ";

  // At iteration 0 the temperature is Inf, and hence theres no weighting!
  temp(0) = (det_schedule>0)?20000.0:20000.0*n; ess = n_part; cess = n_part;
  int count_stuck = 0;
  double prior_weight_proposal = 1e-2*count_stuck;
  
  lw = zeros(n_part);
  
  rowvec tmp_z; //used later in the transd-move
  vec ll_tmp(n_part);// vec p_tmp(n_part); vec w_tmp(n_part);
  double tmp_ll;
  
  //compute the starting prior value (note the likelihood is not in the picture at the moment, but I compute it to keep track of it)
  #pragma omp parallel for
  for(int j=0; j<n_part; ++j){
    lprior_curr(j) = NN_lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
    ll_curr(j) = conditional_t_NN_lL(y,X,Z.slice(j),exp(lsigma2_y(j)),exp(lp_y(j)),temp(i));
  }

  std::cout << "Done!" << std::endl;
  
  // ******************************************
  // *********  Step 1 ... and on ...  ********
  // ******************************************
  
  
  while(temp(i) > 1){
    
    // increment iteration count
    i++;
    
    if(i >= temp.n_elem){ 
      temp.resize(2*temp.n_elem);
      essCount.resize(2*essCount.n_elem);
    }

    
    cout << std::endl << "######## Iteration " << i << " " << endl;


    ll_tmp = ll_curr; //starting point for ll_tmp
    
    //find the appropriate temp
    std::cout << "Adapting temperature ladder: ";  
    
    if(det_schedule==0) temp(i) = find_temp_cESS(lW,ll_tmp,temp(i-1),cess,cess*factor_temp); // Adaptive
      else if(det_schedule == 1) temp(i) = std::max(factor_temp*temp(i-1),1.); // Exponential
           else if(det_schedule == 2) temp(i) = std::max(temp(i-1)-factor_temp,1.); //Linear (CARE, totally worthless just placeholder!)
                else{
                  std::cout << "Deterministic schedule with undefined value, defaulting to the exponential one." << std::endl;
                  det_schedule = 1;
                  temp(i) = std::max(factor_temp*temp(i-1),1.);
                }
    
    if( temp(i-1) == temp(i)){

      if(count_stuck<10){
        //increment proposal! (by using count_stuck)
        std::cout << std::endl << "CARE: temperature ladder stuck, increasing the proposal variance to facilitate far moves!";
        ++count_stuck;
      }else{
        count_stuck=0;
        temp(i) = std::max( temp(i)-1e-4*temp(i) , 1.);
        std::cout << std::endl << "CARE: temperature ladder forced to move, may results in unexpected behaviours!" ;
      }

    }else{
      count_stuck=0; // temp is moving, all good
    } 

    if( i > 101 ){ if((temp(i-100) - temp(i)) <= 1e-2*temp(i-100) ){
      std::cout << std::endl<< "The temperature schedule got stuck, try with more particles or more informative priors..." << 
      std::endl << "Meaning I could force the temp to decrease but it would only lead to degeneracy..." << std::endl;
      return Rcpp::wrap(0.); 
    } } 
    
    std::cout << std::endl << "Next Temp = "<< temp(i)<<" ... "; 
    
    //******************************************
    //******************* re-weight ************
    //****************************************** 
    
    // Reweight the particles, equation (31) Del Moral 2005 [done here as it involves just the particles at time i-]
    std::cout << "Reweighting ... ";  
    
    // ***** Weights ********
    
    /*  math
              lik ^ 1/temp_i
    log ( ---------------------  ) -> log(lik)/temp_i - log(lik)/temp_(i-1)
            lik ^ 1/temp_(i-1)
    
    since ll = log(lik)/temp_(i-1) (ie ll is already tempered for temp(i-1)) we got log(lik) = ll * temp_(i-1)
    
    -> ll*temp_(i-1)/temp_i - ll  --> ll * ( temp_(i-1)/temp_i -1 )
    */
    
    lw = lW + ( temp(i-1)/temp(i) -1. ) * ll_tmp; //note that with this tempering scheme the prior remains invariant, hence does not enter in the reweighting
    
    // ***** ESS ********
    ess = ESS_log(lw); // new ESS with the un-Normalized weights!
    // ***** cESS ********
    cess = cESS_log( ( temp(i-1)/temp(i) -1. ) * ll_tmp , lW); // care for caps W !! lw is the newly incremental log-UNnormalized w, while lW is the old log-Normalized (still there's a check in cESS_log)

    // Normalize the (LOG_)WEIGHTS
    sum_lw = lw(0);
    for(int j=1;j<n_part;j++) sum_lw = log_add(sum_lw,lw(j));
    
    lW = lw - sum_lw;
    W = exp(lW); 

    // here we have already a sample from the target_i, all successive MCMC moves are invariant wrt target_i
    std::cout << "Done!";
    
    
    //************************************************
    //**** RESAMPLING ********************************    
    //************************************************
    
    
    // ***** Complete Degeneracy?
    if(!std::isfinite(ess) || ess==0){
      std::cout << "Complete Degeneracy of the particles! Too bad...";
      
      temp.resize(i);
      return Rcpp::wrap(0);
      
    } // otherwise continue as normal
    
    std::cout << " ESS = " <<ess<< std::endl;
    
    if(ess < thresh /*|| temp(i) ==1*/ ){ // particles heading toward degeneracy / last resample
      
      //******************************************
      //**** RESAMPLING *** (multinomial resampling) ***     //to keep it clean put it in another function
      //******************************************
      
      std::cout << "Resampling ... " ;  
      
      idx = resid_resample(n_part,W);
      
      //**** ACTUAL RESAMPLING ***
      
      tmp_Z = Z;  
      #pragma acc parallel loop
      for(int j=0; j<n_part; ++j){ 
        Z.slice(j) = tmp_Z.slice(idx(j)); 
      } //hopefully working as intended...threadsafe as working on 2 copies. Need the loop as of now, no non-adjacent index
      
      lsigma2_z = lsigma2_z.elem(idx);
      lsigma2_y = lsigma2_y.elem(idx);
      lp_y = lp_y.elem(idx);
      lp_z = lp_z.elem(idx);
      ll_curr = (temp(i-1)/temp(i))*ll_tmp.elem(idx);
      lw = zeros(n_part);
      W = ones(n_part)/n_part;
      lW = log(W);
      ess = n_part;
      cess = n_part;
      
      std::cout << "Resampled!" << std::endl;  
      
      
    }else{ 
      
      // No Degeneracy = No resample! 
      
    }
    
    
    //******************************************
    //**** MCMC moves ***
    //******************************************
    
    //Set hyperparameters
    
    factor_sd = 2.38; //assuming gaussian, see Roberts 96, would be 2.38 something...actually a bit less for the optimal acc rate in dim 4 is less then 0.234
    // in our case though the variance is computed on a set that comes from a previous iter, even though is actually resampled to get to the current temp
    // so maybe a bit less would make sense...

   // weighted covariance matrix too slow.
    Sigma_prop(0,0) = w_var(lsigma2_y,W);
    Sigma_prop(1,1) = w_var(lp_y,W);
    Sigma_prop(2,2) = w_var(lsigma2_z,W);
    Sigma_prop(3,3) = w_var(lp_z,W);
    
    Sigma_prop(0,1) = w_cov(lsigma2_y,lp_y,W);
    Sigma_prop(0,2) = w_cov(lsigma2_y,lsigma2_z,W);
    Sigma_prop(0,3) = w_cov(lsigma2_y,lp_z,W);
    
    Sigma_prop(1,2) = w_cov(lp_y,lsigma2_z,W);
    Sigma_prop(1,3) = w_cov(lp_y,lp_z,W);
    
    Sigma_prop(2,3) = w_cov(lsigma2_z,lp_z,W);
    
    Sigma_prop(1,0) = Sigma_prop(0,1);
    Sigma_prop(2,0) = Sigma_prop(0,2);
    Sigma_prop(3,0) = Sigma_prop(0,3);
    Sigma_prop(2,1) = Sigma_prop(1,2);
    Sigma_prop(3,1) = Sigma_prop(1,3);
    Sigma_prop(3,2) = Sigma_prop(2,3);
    
    // unweighted, but if we just resampled...then find nearest valid!
    theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z);
    // Sigma_prop = cov(theta);

    Sigma_prop = factor_sd * Sigma_prop; 
    
    //could also be somethig like 
    //Sigma_prop = 0.5*old_Sigma_prop + 0.5*Sigma_prop
    //but that would defy te fct that the optimal scaling is higher than both S_prop and old_
    
    // could still target an optimal A/R ratio in the end phase but that would lead to suboptimal scaling in the high temperature...
    // instead we rescale with the prior variance if  the temp ladder seems stuck
    prior_weight_proposal = 1e-2*count_stuck; 
    Sigma_prop = (1.-prior_weight_proposal)*Sigma_prop + prior_weight_proposal * Sigma_prop_0;


    if( !chol(chol_Sigma_prop,Sigma_prop) ){
      std::cout << " !! ";                        // find nearest covariance matrix to the one fond there TODO
      
      //TODO better, this is just a patch
      eig_sym(tmp_eigval,tmp_eigvec,Sigma_prop,"std");
      for(int ii=0; ii<4; ++ii){
        if( tmp_eigval(ii) <= 0) tmp_eigval(ii) = 1e-5;
      }
      Sigma_prop = tmp_eigvec * diagmat(tmp_eigval) * trans(tmp_eigvec);      

      if( !chol(chol_Sigma_prop,Sigma_prop) ){  // ... never lucky
        std::cout << " ?? ";                       
        Sigma_prop = old_Sigma_prop;
        chol(chol_Sigma_prop,Sigma_prop);
      }
    }      
  
    // M_ultiple MCMC step
    for(int mcmc_i=0; mcmc_i < M; ++mcmc_i){
      
      std::cout << "Gibbs step #" <<mcmc_i+1 << "  ";  
      
      //***** Update for log-parameters  ***** phi , sigma, lsigma2_z | Z (rw proposals)
      
      
      tmp_mat = normal_01(n_part*4);
      tmp_mat.resize(n_part,4);
      tmp_mat = tmp_mat * chol_Sigma_prop;
      
      // theta = join_horiz(join_horiz(join_horiz(lsigma2_y,lp_y),lsigma2_z),lp_z); SUPERFLUOUS
      theta_new = theta + tmp_mat;
      
      //**** Joint Acceptance rate ****
      
      // Posterior ratio
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        
        ll_new(j) = conditional_t_NN_lL(y,X,Z.slice(j),exp(theta_new(j,0)),exp(theta_new(j,1)),temp(i));
        lprior_new(j) = NN_lp(Z.slice(j),p,exp(theta_new(j,0)),exp(theta_new(j,1)),exp(theta_new(j,2)),exp(theta_new(j,3)),
                   shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
      }
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) = (ll_new(j) + lprior_new(j)) - (ll_curr(j) + lprior_curr(j));
      }
      
      #pragma omp parallel for
      for(int j=0;j<n_part;j++){
        if( std::isnan(acc_rate(j)) || !std::isfinite(acc_rate(j))) acc_rate(j) = -DBL_MAX;
      }
      
      //proposal correction (the proposal on log-sigma2 is symmetric)
      
      //jacobian of exp(theta) -- log of q(new->old)/q(old->new) * exp(new)/exp(old) when q is symmetric
      // NB, the change of variable is in the posterior..
      #pragma acc parallel loop
      for(int j=0;j<n_part;j++){
        acc_rate(j) += (theta_new(j,0) - theta(j,0)) + (theta_new(j,1) - theta(j,1)) + (theta_new(j,2) - theta(j,2)) + (theta_new(j,3) - theta(j,3)); 
      }
      
      old_Sigma_prop = Sigma_prop;
      
      std::cout << "Updating... ";  
      
      
      // ***** Finally Updating *****  would it be faster with a parallel loop?
      
      rand = unif_01(n_part);
      idx = find(log(rand) < acc_rate);
      n_idx = find(log(rand) >= acc_rate);
      
      theta.rows(idx) = theta_new.rows(idx);

      ll_curr.elem(idx) = ll_new.elem(idx);
      lprior_curr.elem(idx) = lprior_new.elem(idx);

      lsigma2_y = theta.col(0);
      lp_y = theta.col(1);
      lsigma2_z = theta.col(2);
      lp_z = theta.col(3);

      
      // Z s are not changed here
      
      std::cout << "done w/ acc_rate = " << (double)idx.n_elem /n_part << endl;  

      //weights stay the same
      
    } //end multiple MCMC steps
    
    if(p>0){

      //******************************************
      //**** Elliptical Slice Sampler Moves ****  Z | phi and sigma2
      //******************************************

      essCount(i) = 0;
      
      for(int jj=0;jj<J;jj++){ // for more then one step...
        
        std::cout << "Ell_SS step #" <<jj+1 << " ... ";  
        
        #pragma omp parallel for
        for(int j=0; j<n_part; ++j){ //every particle
          
          Z.slice(j) = conditional_NN_elliptical_ss( Z.slice(j) , ll_curr(j), Dx, exp(lsigma2_z(j)) , exp(lp_z(j)) ,y,n,X, exp(lsigma2_y(j)),exp(lp_y(j)) , temp(i),approx_order_prior,Nsets_prior, essCount(i));
          
          // Note that ll_curr is updated by passing it by reference (I could actually do the same for Z)
          // lprior needs to be updated though 
          lprior_curr(j) = NN_lp(Z.slice(j),p,exp(lsigma2_y(j)),exp(lp_y(j)),exp(lsigma2_z(j)),exp(lp_z(j)),shape_p_y,rate_p_y,shape_s_y,rate_s_y,shape_p_z,rate_p_z,shape_s_z,rate_s_z,Dx,approx_order_prior,Nsets_prior);
          
        } // loop for every particle
      } // loop J times EllSliceSsamp
      
      essCount(i) = ((essCount(i) / n_part) / J )/ p ;
      std::cout << "(avg#steps " << essCount(i) << ") -- Done!" << endl;

      
      // Note there is no need to zero out the rest as for now the dimension is fixed
      
    } // if p > 0 do the EllSS move, otherwise: Nothing to do, do not change Z and ll and lp stay the same
    
    
    //  ********* END full-MCMC MOVE
    
    // Visual Output per iteration
    
    /* cout << "(M_s_y= "<< mean(exp(lsigma2_y))<<", M_p_y= "<< mean(exp(lp_y))<<", M_s_z="<< as_scalar(mean(exp(lsigma2_z))) << ") " <<endl<<"    ";
    std::cout << "([Log]SD_s_y= "<< sqrt(Sigma_prop(0,0)) <<", [Log]SD_p= "<< sqrt(Sigma_prop(1,1)) << ") "<<", [Log]SD_s_z= "<< sqrt(Sigma_prop(2,2)) << ") "<<std::endl;
    */
    
    //temp(50) = 1; //for debugging purposes ! keep commented -- remember usleep(100); 
    
  }  // end SMC while
  
  std::cout<<"SMC_TEMPERING Done!"<<std::endl;
  
  // ******* Posterior Covariance
  //Compute the resulting Covariance Matrix so we can keep track of it and output that
  // I could speed this up by saving it in the last ll computation!
  
  // #pragma omp parallel for default(shared)
  // for(int j=0; j<n_part; ++j){
  //   post_Dxz.slice(j) = distMat(join_rows(X,trans(Z.slice(j))));
  //   post_Exz.slice(j) = matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),post_Dxz.slice(j)); //cant I just translate X at the start?
  // }
  
  vec marg_lik_vec(n_part);
  marg_lik_vec(0) = as_scalar(lW(0)+ll_curr(0));
  double marg_likelihood_est = marg_lik_vec(0);
  
  #pragma omp parallel for default(shared)
  for(int j=1; j<n_part; ++j){
    marg_lik_vec(j) = as_scalar(lW(j)+ll_curr(j));
    marg_likelihood_est = log_add(marg_lik_vec(j),marg_likelihood_est);
  }                                                                         //this gives me the logarithm of the marginal likelihood estimator TODO STABILIZE FOR NUMERICAL APPROX!!!
   
  double marg_likelihood_est2 = vec_log_add(marg_lik_vec);
  
  temp.resize(i);
  essCount.resize(i);

  mat post_mean_Exz = zeros<mat>(s,s);
  #pragma omp parallel for default(shared)
  for(int j=0; j<n_part; ++j){
    post_mean_Exz = post_mean_Exz + matern(ni_likelihood,exp(lsigma2_y(j)),exp(lp_y(j)),distMat(join_rows(X,trans(Z.slice(j))))); 
  }
  post_mean_Exz = post_mean_Exz/(double)n_part;

  Rcpp::List out = Rcpp::List::create(/*Rcpp::Named("post_Exz")=post_Exz, Rcpp::Named("post_Dxz")=post_Dxz */
                            Rcpp::Named("post_mean_Exz")=post_mean_Exz,Rcpp::Named("p")=p,Rcpp::Named("Z")=Z,
                                                  Rcpp::Named("log_sigma2_y")=lsigma2_y,Rcpp::Named("log_phi_y")=lp_y,Rcpp::Named("log_sigma2_z")=lsigma2_z,Rcpp::Named("ll_final")=ll_curr,
                                                  Rcpp::Named("log_phi_z")=lp_z, Rcpp::Named("W")=W,Rcpp::Named("lW")=lW,
                                                  Rcpp::Named("log_marg_likelihood_est")=marg_likelihood_est,Rcpp::Named("log_marg_likelihood_est2")=marg_likelihood_est2,
                                                  Rcpp::Named("marg_lik_vec")=marg_lik_vec, Rcpp::Named("T")=temp, Rcpp::Named("avg_ess_cout") = mean(essCount));
  return out;
  
}

/* #########################################################################
####################    Dim Exp  Wrapper function    #######################
############################################################################ */


// [[Rcpp::export]]
Rcpp::List DE_sampler(int n_part, mat X, mat data, int n ,int p, double factor_temp_or_target_ar , double thresh=-1, int deterministic_temp_schedule=0, int M=1, int J=1,
                        double shape_p_y = -1., double rate_p_y = -1. ,double shape_s_y = -1. , double rate_s_y=-1. ,
                        double shape_p_z = 0, double rate_p_z = 0,double shape_s_z = 0, double rate_s_z = 0)
{
  
  if( shape_p_y < 0  ||  rate_p_y < 0 || shape_s_y < 0 || rate_s_y < 0 ){
    std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
    return Rcpp::List::create(shape_p_y);
  }
  
  mat SampleCov; vec y_bar;
  
  if( is_cov_matrix(data) ){
    cout << "Detected Symmetric (Covariance?) Matrix" << endl << 
          "Note that with only the covariance the marginal likelihood etimator might be slightly biased..." << endl <<
          "To be sure of the results either input the original data or (1/n) Sum_i^n (Y_i * Y'_i) " <<endl;
  // if the user passed a covariance matrix (note we've no way to check n here)
    SampleCov = data;
    y_bar = zeros<vec>(data.n_rows);
  }else{
  // if the user passed a data matrix
    cout << "Detected data Matrix" << endl;
    SampleCov = cov(data,1);
    y_bar = mean(data).t();
    if( n != data.n_rows ){ // prevent weird initialization of n
      n = data.n_rows;
      std::cout << "WARNING: n, the sample size for the data, has been reset to the number of rows of the parameter 'data'. check your input." << std::endl; //still warn user
    }
  }
  
  if( p >= 0 ){ // Trigger SMC for fixed p
    
    cout << "Starting fixed Dimension SMC for p="<<p << endl;
    
    if( thresh < 0 ) thresh = n_part*0.95;

      if( p > 0 ){

          if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
            std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
            return Rcpp::List::create(shape_p_z);
          }
          return DE_SMC(n_part,X,SampleCov,y_bar,n,p,
                    factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                    shape_p_y,rate_p_y,shape_s_y,rate_s_y,
                    shape_p_z,rate_p_z,shape_s_z,rate_s_z);
      }else{
          return baseline_SMC(n_part,X,SampleCov,y_bar,n,
                    factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                    shape_p_y,rate_p_y,shape_s_y,rate_s_y);
      }

  }else{ // negative p trigger transdimensional MCMC
    
    cout << " Starting RJMCMC " << endl;
    
          if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
            std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
            return Rcpp::List::create(shape_p_z);
          }
          return DE_MCMC(n_part,X,SampleCov,y_bar,n,factor_temp_or_target_ar,M,J,    // note there's no baseline model encoded yet, as the RJ transition between p=0 and p>0 is ... weird
                    shape_p_y,rate_p_y,shape_s_y,rate_s_y,
                    shape_p_z,rate_p_z,shape_s_z,rate_s_z);

  }
  
}


// [[Rcpp::export]]
Rcpp::List DE_predictive_sampler(int n_part, mat X, mat data, int n ,int p, int s_star, double factor_temp_or_target_ar , double thresh=-1, int deterministic_temp_schedule=0, int M=1, int J=1, int K=0, // s_star is the number of star points
                        double shape_p_y = -1., double rate_p_y = -1. ,double shape_s_y = -1. , double rate_s_y=-1. ,
                        double shape_p_z = 0, double rate_p_z = 0,double shape_s_z = 0, double rate_s_z = 0)
{
  
  if( shape_p_y < 0  ||  rate_p_y < 0 || shape_s_y < 0 || rate_s_y < 0 ){
    std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
    return Rcpp::List::create(shape_p_y);
  }
  
  mat SampleCov; vec y_bar;
  
  if( is_cov_matrix(data) ){
    cout << "Detected Symmetric (Covariance?) Matrix" << endl << 
          "The 'predictive' model cannot run with just the covariance matrix, you need to supply the whole data!" << endl ;
  // if the user passed a covariance matrix (note we've no way to check n here)
        return Rcpp::List::create(shape_p_y);

  }else{
  // if the user passed a data matrix
    cout << "Detected data Matrix" << endl;
    if( n != data.n_rows ){ // prevent weird initialization of n
      n = data.n_rows;
      std::cout << "WARNING: n, the sample size for the data, has been reset to the number of rows of the parameter 'data'. check your input." << std::endl; //still warn user
    }
  }


  // Get ready for the predictive model, given s_star form the grid and 
  int d = X.n_cols;

  int tmp = pow((double)s_star,1./(double)d);
  
  mat X_star = zeros<mat>(s_star,d);
   
  std::cout << "Starting k-means to select the knots ..." << std::endl;
  bool check = arma::kmeans(X_star, X.t(), s_star, random_subset, 100, false );

  if(check){
    X_star = trans(X_star);
  }else{

    std::cout << "Knots selection on a grid  ...";
    // set s_star to grid-sized
  if( round( pow(s_star,1./d) ) != pow(s_star,1./d) ){
    tmp = round( pow((double)s_star,1./(double)d) );
    s_star = pow( tmp, d);
  }
    std::cout << "SET: s_star = "<< s_star << std::endl; //still warn user, even though it might be just the input

  double jnk = 0.05/(double)tmp;

  vec seq( tmp );
  for(int i=0; i<tmp; ++i) seq(i) = jnk + i*(1.-2*jnk)/((double)tmp-1.);

  for(int i=0; i<d; ++i){ 
    for(int j=0; j<s_star; ++j){
      X_star( j , i ) = seq.at( floor( (j / pow(tmp,d-1-i)) )  - tmp*floor( (j / pow(tmp,d-1-(i-1))) ) );
    }}
  }

  for(int i=0; i<d; ++i) X_star.col(i) = X_star.col(i) + 0.02*normal_01(s_star); // slightly jitter to avoid overlap with existing points (which should be possible but has yet to be handled/implemented)
  
  if( p >= 0 ){ // Trigger SMC for fixed p
    
    cout << "Starting fixed Dimension SMC for p="<<p << endl;
    
    if( thresh < 0 ) thresh = n_part*0.95;

    if( p > 0 ){

        if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
          std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
          return Rcpp::List::create(shape_p_z);
        }

        if( K ==0 ){
          return DE_predictive_int_SMC(n_part,data,X,X_star,n,p,s_star,
                  factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                  shape_p_y,rate_p_y,shape_s_y,rate_s_y,
                  shape_p_z,rate_p_z,shape_s_z,rate_s_z);
        }else{
          if(K == 1)
            return DE_predictive_notint_ybar_SMC(n_part,data,X,X_star,n,p,s_star,
                  factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                  shape_p_y,rate_p_y,shape_s_y,rate_s_y,
                  shape_p_z,rate_p_z,shape_s_z,rate_s_z);            
        }

    }else{

        if( K == 0 ){
          return baseline_predictive_int_SMC(n_part,data,X,X_star,n,p,s_star,
                  factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                  shape_p_y,rate_p_y,shape_s_y,rate_s_y);
        }else{
          return baseline_predictive_notint_SMC(n_part,data,X,X_star,n,p,s_star,K,
                  factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                  shape_p_y,rate_p_y,shape_s_y,rate_s_y);
        }
    }

  }else{ // negative p trigger transdimensional MCMC
    
          if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
            std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
            return Rcpp::List::create(shape_p_z);
          }

          std::cout << "MCMC functions not implemented for the predictive model, please use SMC instead." << std::endl;
            return Rcpp::List::create(shape_p_z);
  }
  
}


// [[Rcpp::export]]
Rcpp::List NN_DE_sampler(int n_part, mat X, mat data, int n ,int p, int m, double factor_temp_or_target_ar , double thresh=-1, int deterministic_temp_schedule=0, int M=1, int J=1,
                        double shape_p_y = -1., double rate_p_y = -1. ,double shape_s_y = -1. , double rate_s_y=-1. ,
                        double shape_p_z = 0, double rate_p_z = 0,double shape_s_z = 0, double rate_s_z = 0)
{
  
  if( shape_p_y < 0  ||  rate_p_y < 0 || shape_s_y < 0 || rate_s_y < 0 ){
    std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
    return Rcpp::List::create(shape_p_y);
  }
  
  mat SampleCov; vec y_bar;
  
  if( is_cov_matrix(data) ){
    cout << "Detected Symmetric (Covariance?) Matrix" << endl << 
          "Note that with only the covariance the marginal likelihood etimator might be slightly biased..." << endl <<
          "To be sure of the results either input the original data or (1/n) Sum_i^n (Y_i * Y'_i) " <<endl;

  return Rcpp::List::create(shape_p_y); // NOT IMPLEMENTED YET
          
  // if the user passed a covariance matrix (note we've no way to check n here)
    SampleCov = data;
    y_bar = zeros<vec>(data.n_rows);
  }else{
  // if the user passed a data matrix
    cout << "Detected data Matrix" << endl;
    if( n != data.n_rows ){ // prevent weird initialization of n
      n = data.n_rows;
      std::cout << "WARNING: n, the sample size for the data, has been reset to the number of rows of the parameter 'data'. check your input." << std::endl; //still warn user
    }
  }
  
  if( p >= 0 ){ // Trigger SMC for fixed p
    
    cout << "Starting fixed Dimension SMC for p="<<p << endl;

    GLOBAL_M = m; // change the default value of m, number of NN to consider
    
    if( thresh < 0 ) thresh = n_part*0.95;

      if( p > 0 ){

          if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
            std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
            return Rcpp::List::create(shape_p_z);
          }
          return NN_DE_SMC(n_part,X,data,n,p,
                    factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                    shape_p_y,rate_p_y,shape_s_y,rate_s_y,
                    shape_p_z,rate_p_z,shape_s_z,rate_s_z);
      }else{

          return baseline_NN_DE_SMC(n_part,X,data,n,p,
                    factor_temp_or_target_ar,thresh,deterministic_temp_schedule,M,J,
                    shape_p_y,rate_p_y,shape_s_y,rate_s_y);
      }

  }else{ // negative p trigger transdimensional MCMC
    
    cout << " Starting RJMCMC " << endl;
    return Rcpp::List::create(shape_p_y); // NOT IMPLEMENTED YET


          if( shape_p_z < 0  ||  rate_p_z < 0 || shape_s_z < 0 || rate_s_z < 0 ){
            std::cout << "Negative values or no values given for prior parameters on Y, check your input." << std::endl;
            return Rcpp::List::create(shape_p_z);
          }
          std::cout << "MCMC functions not implemented for the NN model, please use SMC instead." << std::endl;
            return Rcpp::List::create(shape_p_z);

  }
  
}
