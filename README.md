# README #

Normally just run the test.R script in a functioning R environment that satisfies all the dependency to run the procedure on a simulated dataset.

Dependency on CUDA / NVBLAS / OPENAcc  is very thin at the moment. If these are not available on your system just comment out the #include <openacc.h> row in the cpp file.
A smart compiler should ignore consequently all the associated #pragma, if not you should comment out those as well.

### What is this repository for? ###

* Bayesian Dimension Expansion procedure to model nonstationary processes
* Version 0.1.1 -- 14/04/2016

### Contribution guidelines ###

* No "fully fledged" contributions accepted until the code is in a stable state
* test results are welcome

### Who do I talk to? ###

* marco"DOT"banterle"AT"gmail"DOT"com , owner of the repo
